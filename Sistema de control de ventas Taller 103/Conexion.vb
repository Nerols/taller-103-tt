﻿Imports System.Data.SQLite
Public Class Conexion
    Private conexion As New SQLiteConnection
    Private stringConexion As String = "DataSource=TALLER103.db;Password=SGVTALLER103"

    Private Sub AbrirConexion()
        conexion.ConnectionString = stringConexion
        conexion.Open()
    End Sub

    Private Sub CerrarConexion()
        conexion.Close()
    End Sub

    Public Function ConsultaSQL(sql As String)
        Dim command As New SQLiteCommand
        Dim dataset As New DataSet
        Dim reader As New SQLiteDataAdapter

        AbrirConexion()

        command.Connection = conexion
        command.CommandText = sql

        reader.SelectCommand = command
        reader.Fill(dataset)

        CerrarConexion()

        Return dataset.Tables(0)
    End Function

    Public Function ComandoSQL(sql As String)
        Dim command As New SQLiteCommand

        AbrirConexion()

        command.Connection = conexion
        command.CommandText = sql

        command.ExecuteNonQuery()

        CerrarConexion()

        Return True
    End Function
End Class
