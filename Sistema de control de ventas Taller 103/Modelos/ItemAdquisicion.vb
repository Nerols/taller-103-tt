﻿Public Class ItemAdquisicion
    Protected _id As Integer
    Protected _idProducto As Integer
    Protected _idAdquisicion As Integer
    Protected _cantidad As Integer
    Protected _costo As Integer

    Property id As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    Property idProducto As Integer
        Get
            Return _idProducto
        End Get
        Set(value As Integer)
            _idProducto = value
        End Set
    End Property

    Property idAdquisicion As Integer
        Get
            Return _idAdquisicion
        End Get
        Set(value As Integer)
            _idAdquisicion = value
        End Set
    End Property

    Property cantidad As Integer
        Get
            Return _cantidad
        End Get
        Set(value As Integer)
            _cantidad = value
        End Set
    End Property

    Property costo As Integer
        Get
            Return _costo
        End Get
        Set(value As Integer)
            _costo = value
        End Set
    End Property
End Class
