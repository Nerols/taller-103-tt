﻿Public Class Venta
    Protected _id As Integer
    Protected _idCliente As String
    Protected _fechaEmision As Date
    Protected _fechaEntrega As Date
    Protected _fechaCompromiso As Date
    Protected _borradoEn As Date

    Property id As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    Property idCliente As String
        Get
            Return _idCliente
        End Get
        Set(value As String)
            _idCliente = value
        End Set
    End Property

    Property fechaEmision As Date
        Get
            Return _fechaEmision
        End Get
        Set(value As Date)
            _fechaEmision = value
        End Set
    End Property

    Property fechaEntrega As Date
        Get
            Return _fechaEntrega
        End Get
        Set(value As Date)
            _fechaEntrega = value
        End Set
    End Property

    Property fechaCompromiso As Date
        Get
            Return _fechaCompromiso
        End Get
        Set(value As Date)
            _fechaCompromiso = value
        End Set
    End Property

    Property borradoEn As Date
        Get
            Return _borradoEn
        End Get
        Set(value As Date)
            _borradoEn = value
        End Set
    End Property
End Class
