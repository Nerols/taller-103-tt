﻿Public Class Cliente
    Protected _id As Integer
    Protected _rut As String
    Protected _nombre As String
    Protected _apellido As String
    Protected _fono1 As String
    Protected _fono2 As String
    Protected _email As String
    Protected _direccion As String
    Protected _borradoEn As Date

    Property id As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    Property rut As String
        Get
            Return _rut
        End Get
        Set(value As String)
            _rut = value
        End Set
    End Property

    Property nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

    Property apellido As String
        Get
            Return _apellido
        End Get
        Set(value As String)
            _apellido = value
        End Set
    End Property

    Property fono1 As String
        Get
            Return _fono1
        End Get
        Set(value As String)
            _fono1 = value
        End Set
    End Property

    Property fono2 As String
        Get
            Return _fono2
        End Get
        Set(value As String)
            _fono2 = value
        End Set
    End Property

    Property email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Property direccion As String
        Get
            Return _direccion
        End Get
        Set(value As String)
            _direccion = value
        End Set
    End Property

    Property borradoEn As Date
        Get
            Return _borradoEn
        End Get
        Set(value As Date)
            _borradoEn = value
        End Set
    End Property
End Class
