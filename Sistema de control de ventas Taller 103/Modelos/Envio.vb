﻿Public Class Envio
    Protected _id As Integer
    Protected _fechaEnvio As Date
    Protected _precio As Integer
    Protected _numeroEnvio As String
    Protected _courier As String
    Protected _descripcion As String
    Protected _idVenta As Integer

    Property id As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    Property fechaEnvio As Date
        Get
            Return _fechaEnvio
        End Get
        Set(value As Date)
            _fechaEnvio = value
        End Set
    End Property

    Property precio As Integer
        Get
            Return _precio
        End Get
        Set(value As Integer)
            _precio = value
        End Set
    End Property

    Property numeroEnvio As String
        Get
            Return _numeroEnvio
        End Get
        Set(value As String)
            _numeroEnvio = value
        End Set
    End Property

    Property courier As String
        Get
            Return _courier
        End Get
        Set(value As String)
            _courier = value
        End Set
    End Property

    Property descripcion As String
        Get
            Return _descripcion
        End Get
        Set(value As String)
            _descripcion = value
        End Set
    End Property

    Property idVenta As Integer
        Get
            Return _idVenta
        End Get
        Set(value As Integer)
            _idVenta = value
        End Set
    End Property
End Class
