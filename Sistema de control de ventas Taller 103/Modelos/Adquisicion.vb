﻿Public Class Adquisicion
    Protected _id As Integer
    Protected _idProveedor As Integer
    Protected _fechaAdquisicion As Date
    Protected _borradoEn As Date

    Property id As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    Property idProveedor As Integer
        Get
            Return _idProveedor
        End Get
        Set(value As Integer)
            _idProveedor = value
        End Set
    End Property

    Property fechaAdquisicion As Date
        Get
            Return _fechaAdquisicion
        End Get
        Set(value As Date)
            _fechaAdquisicion = value
        End Set
    End Property
    Property borradoEn As Date
        Get
            Return _borradoEn
        End Get
        Set(value As Date)
            _borradoEn = value
        End Set
    End Property

End Class
