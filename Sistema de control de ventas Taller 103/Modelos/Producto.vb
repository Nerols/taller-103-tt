﻿Public Class Producto
    Protected _id As Integer
    Protected _idTipo As Integer
    Protected _nombre As String
    Protected _precio As Integer
    Protected _costo As Integer
    Protected _stock As Integer
    Protected _stockComp As Integer
    Protected _borradoEn As Date

    Property id As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    Property idTipo As Integer
        Get
            Return _idTipo
        End Get
        Set(value As Integer)
            _idTipo = value
        End Set
    End Property

    Property nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

    Property precio As Integer
        Get
            Return _precio
        End Get
        Set(value As Integer)
            _precio = value
        End Set
    End Property

    Property costo As Integer
        Get
            Return _costo
        End Get
        Set(value As Integer)
            _costo = value
        End Set
    End Property

    Property stock As Integer
        Get
            Return _stock
        End Get
        Set(value As Integer)
            _stock = value
        End Set
    End Property

    Property stockComp As Integer
        Get
            Return _stockComp
        End Get
        Set(value As Integer)
            _stockComp = value
        End Set
    End Property

    Property borradoEn As Date
        Get
            Return _borradoEn
        End Get
        Set(value As Date)
            _borradoEn = value
        End Set
    End Property
End Class
