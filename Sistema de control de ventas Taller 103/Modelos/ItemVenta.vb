﻿Public Class ItemVenta
    Protected _id As Integer
    Protected _idProducto As Integer
    Protected _idVenta As Integer
    Protected _cantidad As Integer
    Protected _precioVenta As Integer
    Protected _costoVenta As Integer
    Protected _descripcion As String
    Protected _cantidadComp As Integer

    Property id As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    Property idProducto As Integer
        Get
            Return _idProducto
        End Get
        Set(value As Integer)
            _idProducto = value
        End Set
    End Property

    Property idVenta As Integer
        Get
            Return _idVenta
        End Get
        Set(value As Integer)
            _idVenta = value
        End Set
    End Property

    Property cantidad As Integer
        Get
            Return _cantidad
        End Get
        Set(value As Integer)
            _cantidad = value
        End Set
    End Property

    Property precioVenta As Integer
        Get
            Return _precioVenta
        End Get
        Set(value As Integer)
            _precioVenta = value
        End Set
    End Property

    Property costoVenta As Integer
        Get
            Return _costoVenta
        End Get
        Set(value As Integer)
            _costoVenta = value
        End Set
    End Property

    Property descripcion As String
        Get
            Return _descripcion
        End Get
        Set(value As String)
            _descripcion = value
        End Set
    End Property

    Property cantidadComp As Integer
        Get
            Return _cantidadComp
        End Get
        Set(value As Integer)
            _cantidadComp = value
        End Set
    End Property
End Class
