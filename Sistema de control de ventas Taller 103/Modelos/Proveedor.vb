﻿Public Class Proveedor
    Protected _id As Integer
    Protected _rut As String
    Protected _nombre As String
    Protected _descripcion As String
    Protected _fono As String
    Protected _email As String
    Protected _sitio As String
    Protected _borradoEn As Date

    Property id As Integer
        Get
            Return _id
        End Get
        Set(value As Integer)
            _id = value
        End Set
    End Property

    Property rut As String
        Get
            Return _rut
        End Get
        Set(value As String)
            _rut = value
        End Set
    End Property

    Property nombre As String
        Get
            Return _nombre
        End Get
        Set(value As String)
            _nombre = value
        End Set
    End Property

    Property descripcion As String
        Get
            Return _descripcion
        End Get
        Set(value As String)
            _descripcion = value
        End Set
    End Property

    Property fono As String
        Get
            Return _fono
        End Get
        Set(value As String)
            _fono = value
        End Set
    End Property

    Property email As String
        Get
            Return _email
        End Get
        Set(value As String)
            _email = value
        End Set
    End Property

    Property sitio As String
        Get
            Return _sitio
        End Get
        Set(value As String)
            _sitio = value
        End Set
    End Property

    Property borradoEn As Date
        Get
            Return _borradoEn
        End Get
        Set(value As Date)
            _borradoEn = borradoEn
        End Set
    End Property
End Class
