﻿Public Class DAOControlDeProducto
    Dim conexion As New Conexion

    Public Function GetControlesDeProductos()
        Dim sql As String = "
            SELECT
            CONTROLES_PRODUCTOS.ID,
            PRODUCTOS.NOMBRE AS [Producto],
            CONTROLES_PRODUCTOS.FECHA AS [Fecha],
            CONTROLES_PRODUCTOS.TIPO AS [Tipo],
            CONTROLES_PRODUCTOS.DESCRIPCION [Descripción],
            CONTROLES_PRODUCTOS.CANTIDAD AS [Cantidad]
            FROM CONTROLES_PRODUCTOS
            INNER JOIN PRODUCTOS ON CONTROLES_PRODUCTOS.ID_PRODUCTO = PRODUCTOS.ID"

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetControlDeProducto(id As String)
        Dim sql As String = "SELECT * FROM CONTROLES_PRODUCTOS WHERE ID = " & id

        Return conexion.ConsultaSQL(sql).Rows(0)
    End Function

    Public Function BusquedaControlDeProducto(busqueda As String)
        Dim sql As String = "
            SELECT
            CONTROLES_PRODUCTOS.ID,
            PRODUCTOS.NOMBRE AS [Producto],
            CONTROLES_PRODUCTOS.FECHA AS [Fecha],
            CONTROLES_PRODUCTOS.TIPO AS [Tipo],
            CONTROLES_PRODUCTOS.DESCRIPCION [Descripción],
            CONTROLES_PRODUCTOS.CANTIDAD AS [Cantidad]
            FROM CONTROLES_PRODUCTOS
            INNER JOIN PRODUCTOS ON CONTROLES_PRODUCTOS.ID_PRODUCTO = PRODUCTOS.ID
            WHERE PRODUCTOS.NOMBRE LIKE '" & busqueda & "%'"
        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetControlesDeProducto(idProducto As String)
        Dim sql As String = "SELECT * FROM CONTROLES_PRODUCTOS WHERE ID_PRODUCTO = " & idProducto

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function CrearControlDeProducto(controlProducto As ControlDeProducto)
        Dim sql As String =
            "INSERT INTO CONTROLES_PRODUCTOS (ID_PRODUCTO, FECHA, TIPO, DESCRIPCION, CANTIDAD)
            VALUES (" & controlProducto.idProducto & ", '" &
            controlProducto.fecha.ToString("yyyy-MM-dd") & "', '" & controlProducto.tipo & "', '" &
            controlProducto.descripcion & "', " & controlProducto.cantidad & ")"

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EditarControlDeProducto(controlProducto As ControlDeProducto)
        Dim sql As String =
            "UPDATE CONTROLES_PRODUCTOS SET ID_PRODUCTO = " & controlProducto.idProducto &
            ", FECHA = '" & controlProducto.fecha.ToString("yyyy/MM/dd") & "', TIPO = '" &
            controlProducto.tipo & ", DESCRIPCION = '" & controlProducto.descripcion &
            ", CANTIDAD = " & controlProducto.cantidad & " WHERE ID = " & controlProducto.id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EliminarControlDeProducto(id As String)
        Dim sql As String = "DELETE FROM CONTROLES_PRODUCTOS WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function
End Class
