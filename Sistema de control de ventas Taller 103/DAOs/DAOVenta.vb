﻿Public Class DAOVenta
    Dim conexion As New Conexion

    Public Function GetVentas()
        Dim sql As String = "
            SELECT
            VENTAS.ID AS [Número Venta],
            (CLIENTES.NOMBRE || ' ' || CLIENTES.APELLIDO) AS [Nombre Cliente],
            VENTAS.FECHA_EMISION AS [Fecha emisión],
            VENTAS.FECHA_ENTREGA AS [Fecha entrega],
            VENTAS.FECHA_COMPROMISO AS [Fecha compromiso],
            VENTAS.BORRADO_EN AS [Fecha Borrado]
            FROM VENTAS
            INNER JOIN CLIENTES ON VENTAS.ID_CLIENTE = CLIENTES.ID
            WHERE VENTAS.BORRADO_EN IS NULL
            "

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetAllVentas()
        Dim sql As String = "
            SELECT
            VENTAS.ID AS [Número Venta],
            (CLIENTES.NOMBRE || ' ' || CLIENTES.APELLIDO) AS [Nombre Cliente],
            VENTAS.FECHA_EMISION AS [Fecha emisión],
            VENTAS.FECHA_ENTREGA AS [Fecha entrega],
            VENTAS.FECHA_COMPROMISO AS [Fecha compromiso],
            VENTAS.BORRADO_EN AS [Fecha Borrado]
            FROM VENTAS
            INNER JOIN CLIENTES ON VENTAS.ID_CLIENTE = CLIENTES.ID
            "

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetVenta(id As String)
        Dim sql As String = "SELECT * FROM VENTAS WHERE ID = " & id

        Return conexion.ConsultaSQL(sql).Rows(0)
    End Function

    Public Function BusquedaVenta(busqueda As String)
        Dim sql As String = "
            SELECT
            VENTAS.ID AS [Número Venta],
            (CLIENTES.NOMBRE || ' ' || CLIENTES.APELLIDO) AS [Nombre Cliente],
            VENTAS.FECHA_EMISION AS [Fecha emisión],
            VENTAS.FECHA_ENTREGA AS [Fecha entrega],
            VENTAS.FECHA_COMPROMISO AS [Fecha compromiso],
            VENTAS.BORRADO_EN AS [Fecha Borrado]
            FROM VENTAS
            INNER JOIN CLIENTES ON VENTAS.ID_CLIENTE = CLIENTES.ID
            WHERE VENTAS.BORRADO_EN IS NULL AND CLIENTES.NOMBRE LIKE '" & busqueda & "%'"
        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function BusquedaAllVenta(busqueda As String)
        Dim sql As String = "
            SELECT
            VENTAS.ID AS [Número Venta],
            (CLIENTES.NOMBRE || ' ' || CLIENTES.APELLIDO) AS [Nombre Cliente],
            VENTAS.FECHA_EMISION AS [Fecha emisión],
            VENTAS.FECHA_ENTREGA AS [Fecha entrega],
            VENTAS.FECHA_COMPROMISO AS [Fecha compromiso],
            VENTAS.BORRADO_EN AS [Fecha Borrado]
            FROM VENTAS
            INNER JOIN CLIENTES ON VENTAS.ID_CLIENTE = CLIENTES.ID
            WHERE CLIENTES.NOMBRE LIKE '" & busqueda & "%'"
        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetUltVenta()
        Dim sql As String = "SELECT * FROM VENTAS WHERE ID = (SELECT MAX(ID) FROM VENTAS)"

        Return conexion.ConsultaSQL(sql).Rows(0)
    End Function

    Public Function CrearVenta(venta As Venta)
        Dim sql As String =
            "INSERT INTO VENTAS (ID_CLIENTE, FECHA_EMISION, FECHA_ENTREGA, FECHA_COMPROMISO)
            VALUES ('" & venta.idCliente & "', '" & venta.fechaEmision.ToString("yyyy-MM-dd") &
            "', NULL, '" & venta.fechaCompromiso.ToString("yyyy-MM-dd") & "')"

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EstablecerFechaEntrega(venta As Venta)
        Dim sql As String =
            "UPDATE VENTAS SET FECHA_ENTREGA = '" & venta.fechaEntrega.ToString("yyyy-MM-dd") &
            "' WHERE ID = " & venta.id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EliminarVenta(id As String)
        Dim sql As String = "UPDATE VENTAS SET BORRADO_EN = '" & DateTime.Now.ToString("yyyy-MM-dd") & "' WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function RestaurarVenta(id As String)
        Dim sql As String = "UPDATE VENTAS SET BORRADO_EN = NULL WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function
End Class
