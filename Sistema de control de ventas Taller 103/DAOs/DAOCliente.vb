﻿Public Class DAOCliente
    Dim conexion As New Conexion

    Public Function GetClientes()
        Dim sql As String =
            "SELECT
            ID,
            RUT AS [Rut],
            NOMBRE || ' ' || APELLIDO AS [Nombre Completo],
            APELLIDO AS [Apellido],
            FONO1 AS [Fono 1],
            FONO2 AS [Fono 2],
            EMAIL AS [E-mail],
            DIRECCION AS [Dirección],
            BORRADO_EN AS [Fecha Borrado]
            FROM CLIENTES
            WHERE BORRADO_EN IS NULL"

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetAllClientes()
        Dim sql As String = "
            SELECT
            ID,
            RUT AS [Rut],
            NOMBRE || ' ' || APELLIDO AS [Nombre Completo],
            APELLIDO AS [Apellido],
            FONO1 AS [Fono 1],
            FONO2 AS [Fono 2],
            EMAIL AS [E-mail],
            DIRECCION AS [Dirección],
            BORRADO_EN AS [Fecha Borrado]
            FROM CLIENTES"
        Return conexion.ConsultaSQL(sql)
    End Function
    Public Function GetCliente(id As String)
        Dim sql As String = "SELECT * FROM CLIENTES WHERE ID = " & id

        Return conexion.ConsultaSQL(sql).Rows(0)
    End Function

    Public Function BusquedaClienteRut(busqueda As String)
        Dim sql As String = "
            SELECT
            ID,
            RUT AS [Rut],
            NOMBRE || ' ' || APELLIDO AS [Nombre Completo],
            APELLIDO AS [Apellido],
            FONO1 AS [Fono 1],
            FONO2 AS [Fono 2],
            EMAIL AS [E-mail],
            DIRECCION AS [Dirección],
            BORRADO_EN AS [Fecha Borrado]
            FROM CLIENTES 
            WHERE BORRADO_EN IS NULL AND RUT LIKE '" & busqueda & "%'"
        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function BusquedaAllClienteRut(busqueda As String)
        Dim sql As String = "
            SELECT
            ID,
            RUT AS [Rut],
            NOMBRE || ' ' || APELLIDO AS [Nombre Completo],
            APELLIDO AS [Apellido],
            FONO1 AS [Fono 1],
            FONO2 AS [Fono 2],
            EMAIL AS [E-mail],
            DIRECCION AS [Dirección],
            BORRADO_EN AS [Fecha Borrado]
            FROM CLIENTES 
            WHERE RUT LIKE '" & busqueda & "%'"
        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function BusquedaClienteNombre(busqueda As String)
        Dim sql As String = "
            SELECT
            ID,
            RUT AS [Rut],
            NOMBRE || ' ' || APELLIDO AS [Nombre Completo],
            APELLIDO AS [Apellido],
            FONO1 AS [Fono 1],
            FONO2 AS [Fono 2],
            EMAIL AS [E-mail],
            DIRECCION AS [Dirección],
            BORRADO_EN AS [Fecha Borrado]
            FROM CLIENTES 
            WHERE BORRADO_EN IS NULL AND NOMBRE LIKE '" & busqueda & "%'"
        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function BusquedaAllClienteNombre(busqueda As String)
        Dim sql As String = "
            SELECT
            ID,
            RUT AS [Rut],
            NOMBRE || ' ' || APELLIDO AS [Nombre Completo],
            APELLIDO AS [Apellido],
            FONO1 AS [Fono 1],
            FONO2 AS [Fono 2],
            EMAIL AS [E-mail],
            DIRECCION AS [Dirección],
            BORRADO_EN AS [Fecha Borrado]
            FROM CLIENTES 
            WHERE NOMBRE LIKE '" & busqueda & "%'"
        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetClientePorRut(rut As String)
        Dim sql As String = "SELECT * FROM CLIENTES WHERE RUT = '" & rut & "'"
        Dim res = conexion.ConsultaSQL(sql)

        If res.Rows().Count() > 0 Then
            Return res.Rows(0)
        Else
            Return Nothing
        End If
    End Function

    Public Function CrearCliente(cliente As Cliente)
        Dim sql As String = "
            INSERT INTO CLIENTES
            (RUT, NOMBRE, APELLIDO, FONO1, FONO2, EMAIL, DIRECCION)
            VALUES ('" & cliente.rut & "', '" & cliente.nombre & "', '" & cliente.apellido &
            "', '" & cliente.fono1 & "', '" & cliente.fono2 & "', '" & cliente.email & "', '" &
            cliente.direccion & "')"

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EditarCliente(cliente As Cliente)
        Dim sql As String = "
            UPDATE CLIENTES SET RUT = '" & cliente.rut & "', NOMBRE = '" &
            cliente.nombre & "', APELLIDO = '" & cliente.apellido &
            "', FONO1 = '" & cliente.fono1 & "', FONO2 = '" & cliente.fono2 &
            "', EMAIL = '" & cliente.email & "', DIRECCION = '" & cliente.direccion &
            "' WHERE ID = " & cliente.id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EliminarCliente(id As String)
        Dim sql As String = "UPDATE CLIENTES SET BORRADO_EN = '" & DateTime.Now.ToString("yyyy-MM-dd") & "' WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function
    Public Function RestaurarCliente(id As String)
        Dim sql As String = "UPDATE CLIENTES SET BORRADO_EN = NULL WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function
End Class
