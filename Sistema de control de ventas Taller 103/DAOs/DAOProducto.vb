﻿Public Class DAOProducto
    Dim conexion As New Conexion

    Public Function GetProducto(id As String)
        Dim sql As String = "SELECT * FROM PRODUCTOS WHERE ID = " & id

        Return conexion.ConsultaSQL(sql).Rows(0)
    End Function

    Public Function GetProductos()
        Dim sql As String = "
            SELECT 
            PRODUCTOS.ID,
            PRODUCTOS.NOMBRE As [Nombre],
            TIPOS.NOMBRE As [Tipo Producto],
            PRODUCTOS.PRECIO AS [Precio],
            PRODUCTOS.COSTO AS [Costo],
            PRODUCTOS.STOCK AS [Stock],
            PRODUCTOS.STOCK_COMP AS [Stock comprometido],
            PRODUCTOS.BORRADO_EN AS [Fecha Borrado]
            FROM PRODUCTOS
            INNER JOIN TIPOS ON PRODUCTOS.ID_TIPO = TIPOS.ID
            WHERE PRODUCTOS.BORRADO_EN IS NULL
            "

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetAllProductos()
        Dim sql As String = "
            SELECT 
            PRODUCTOS.ID,
            PRODUCTOS.NOMBRE As [Nombre],
            TIPOS.NOMBRE As [Tipo Producto],
            PRODUCTOS.PRECIO AS [Precio],
            PRODUCTOS.COSTO AS [Costo],
            PRODUCTOS.STOCK AS [Stock],
            PRODUCTOS.STOCK_COMP AS [Stock comprometido],
            PRODUCTOS.BORRADO_EN AS [Fecha Borrado]
            FROM PRODUCTOS
            INNER JOIN TIPOS ON PRODUCTOS.ID_TIPO = TIPOS.ID
            "
        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function BusquedaProducto(busqueda As String)
        Dim sql As String = "
            SELECT
            PRODUCTOS.ID,
            PRODUCTOS.NOMBRE As [Nombre],
            TIPOS.NOMBRE As [Tipo Producto],
            PRODUCTOS.PRECIO AS [Precio],
            PRODUCTOS.COSTO AS [Costo],
            PRODUCTOS.STOCK AS [Stock],
            PRODUCTOS.STOCK_COMP AS [Stock comprometido],
            PRODUCTOS.BORRADO_EN AS [Fecha Borrado]
            FROM PRODUCTOS
            INNER JOIN TIPOS ON PRODUCTOS.ID_TIPO = TIPOS.ID
            WHERE PRODUCTOS.BORRADO_EN IS NULL AND PRODUCTOS.NOMBRE LIKE '" & busqueda & "%'"
        Return conexion.ConsultaSQL(sql)
    End Function
    Public Function BusquedaAllProducto(busqueda As String)
        Dim sql As String = "
            SELECT
            PRODUCTOS.ID,
            PRODUCTOS.NOMBRE As [Nombre],
            TIPOS.NOMBRE As [Tipo Producto],
            PRODUCTOS.PRECIO AS [Precio],
            PRODUCTOS.COSTO AS [Costo],
            PRODUCTOS.STOCK AS [Stock],
            PRODUCTOS.STOCK_COMP AS [Stock comprometido],
            PRODUCTOS.BORRADO_EN AS [Fecha Borrado]
            FROM PRODUCTOS
            INNER JOIN TIPOS ON PRODUCTOS.ID_TIPO = TIPOS.ID
            WHERE PRODUCTOS.NOMBRE LIKE '" & busqueda & "%'"
        Return conexion.ConsultaSQL(sql)
    End Function
    Public Function CrearProducto(producto As Producto)
        Dim sql As String =
            "INSERT INTO PRODUCTOS (ID_TIPO, NOMBRE, PRECIO, COSTO, STOCK, STOCK_COMP) VALUES (" &
            producto.idTipo & ", '" & producto.nombre & "', " & producto.precio & ", " &
            producto.costo & ", " & producto.stock & ", 0)"

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EditarProducto(producto As Producto)
        Dim sql As String =
            "UPDATE PRODUCTOS SET ID_TIPO =" & producto.idTipo & ", NOMBRE = '" & producto.nombre &
            "', PRECIO = " & producto.precio & ", COSTO = " & producto.costo & ", STOCK = " &
            producto.stock & ", STOCK_COMP = " & producto.stockComp & " WHERE ID = " & producto.id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EliminarProducto(id As String)

        Dim sql As String = "UPDATE PRODUCTOS SET BORRADO_EN = '" & DateTime.Now.ToString("yyyy-MM-dd") & "' WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function RestaturarProducto(id As String)
        Dim sql As String = "UPDATE PRODUCTOS SET BORRADO_EN = NULL WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function
End Class
