﻿Public Class DAOItemVenta
    Dim conexion As New Conexion

    Public Function GetItemVentaDeProducto(idProducto As String)
        Dim sql As String = "
            SELECT 
            ITEMS_VENTA.ID,
            PRODUCTOS.NOMBRE AS [Nombre de producto],
            ITEMS_VENTA.ID_VENTA AS [Número venta],
            ITEMS_VENTA.CANTIDAD AS [Cantidad],
            ITEMS_VENTA.CANTIDAD_COMP AS [Cantidad comprometida],
            ITEMS_VENTA.PRECIO_VENTA AS [Precio venta],
            ITEMS_VENTA.COSTO_VENTA AS [Costo venta],
            ITEMS_VENTA.DESCRIPCION AS [Descripcion]
            FROM ITEMS_VENTA 
            INNER JOIN PRODUCTOS ON ITEMS_VENTA.ID_PRODUCTO = PRODUCTOS.ID
            WHERE ID_PRODUCTO = " & idProducto

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetItemsVentaDeVenta(idVenta As String)
        Dim sql As String = "
            SELECT 
            ITEMS_VENTA.ID,
            PRODUCTOS.NOMBRE AS [Nombre de producto],
            ITEMS_VENTA.ID_VENTA AS [Número venta],
            ITEMS_VENTA.CANTIDAD AS [Cantidad],
            ITEMS_VENTA.CANTIDAD_COMP AS [Cantidad comprometida],
            ITEMS_VENTA.PRECIO_VENTA AS [Precio venta],
            ITEMS_VENTA.COSTO_VENTA AS [Costo venta],
            ITEMS_VENTA.DESCRIPCION AS [Descripcion]
            FROM ITEMS_VENTA 
            INNER JOIN PRODUCTOS ON ITEMS_VENTA.ID_PRODUCTO = PRODUCTOS.ID
            WHERE ID_VENTA = " & idVenta

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetItemsVentaDeVentaLimpio(idVenta As String)
        Dim sql As String = "
            SELECT 
            *
            FROM ITEMS_VENTA 
            WHERE ID_VENTA = " & idVenta

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function CrearItemVenta(itemVenta As ItemVenta)
        Dim sql As String =
            "INSERT INTO ITEMS_VENTA 
            (ID_PRODUCTO, ID_VENTA, CANTIDAD, CANTIDAD_COMP, PRECIO_VENTA, COSTO_VENTA, DESCRIPCION) 
            VALUES (" & itemVenta.idProducto & ", " & itemVenta.idVenta & ", " & itemVenta.cantidad &
            ", " & itemVenta.cantidadComp & ", " & itemVenta.precioVenta & ", " & itemVenta.costoVenta &
            ", '" & itemVenta.descripcion & "')"

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EditarItemVenta(itemVenta As ItemVenta)
        Dim sql As String =
            "UPDATE ITEMS_VENTA SET ID_PRODUCTO = " & itemVenta.idProducto & ", ID_VENTA = " &
            itemVenta.idVenta & ", CANTIDAD = " & itemVenta.cantidad & ", CANTIDAD_COMP = " & itemVenta.cantidadComp & ", PRECIO_VENTA = " &
            itemVenta.precioVenta & ", COSTO_VENTA = " & itemVenta.costoVenta &
            ", PRECIO_VENTA = " & itemVenta.precioVenta & ", DESCRIPCION = '" &
            itemVenta.descripcion & "' WHERE ID = " & itemVenta.id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EliminarItemVenta(id As String)
        Dim sql As String = "DELETE FROM ITEMS_VENTA WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function
End Class
