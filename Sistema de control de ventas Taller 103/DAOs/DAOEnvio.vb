﻿Public Class DAOEnvio
    Dim conexion As New Conexion

    Public Function GetEnvios()
        Dim sql As String = "
            SELECT 
            ID,
            FECHA_ENVIO AS [Fecha de Envío],
            PRECIO AS [Precio],
            NUMERO_ENVIO AS [Numero de envío],
            COURIER AS [Courier],
            DESCRIPCION AS [Descripción],
            ID_VENTA AS [NO. Venta]
            FROM ENVIOS"

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetEnvio(id As String)
        Dim sql As String = "SELECT * FROM ENVIOS WHERE ID = " & id

        Return conexion.ConsultaSQL(sql).Rows(0)
    End Function

    Public Function BusquedaEnvio(busqueda As String)
        Return True
    End Function

    Public Function CrearEnvio(envio As Envio)
        Dim sql As String =
            "INSERT INTO ENVIOS 
            (FECHA_ENVIO, PRECIO, NUMERO_ENVIO, COURIER, DESCRIPCION, ID_VENTA) VALUES ('" &
            envio.fechaEnvio.ToString("yyyy-MM-dd") & "' , " & envio.precio & " , '" & envio.numeroEnvio &
            "' , '" & envio.courier & "' , '" & envio.descripcion & "' , " & envio.idVenta & ")"

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EditarEnvio(envio As Envio)
        Dim sql As String =
            "UPDATE ENVIOS SET FECHA_ENVIO = '" & envio.fechaEnvio.ToString("yyyy-MM-dd") &
            "', PRECIO = " & envio.precio & ", NUMERO_ENVIO = '" & envio.numeroEnvio &
            "', COURIER = '" & envio.courier & "', DESCRIPCION = '" & envio.descripcion &
            "', ID_VENTA = " & envio.idVenta & " WHERE ID = " & envio.id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EliminarEnvio(id As String)
        Dim sql As String = "DELETE FROM ENVIOS WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function
End Class
