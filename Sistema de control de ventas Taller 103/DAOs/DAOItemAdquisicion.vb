﻿Public Class DAOItemAdquisicion
    Dim conexion As New Conexion
    Public Function GetItemAdquisicionDeProducto(idProducto As String)
        Dim sql As String =
            "SELECT 
            ITEMS_ADQUISICION.ID,
            PRODUCTOS.NOMBRE AS [Nombre de producto],
            ITEMS_ADQUISICION.ID_ADQUISICION AS [Número de adquisición],
            ITEMS_ADQUISICION.CANTIDAD AS [Cantidad],
            ITEMS_ADQUISICION.COSTO AS [Costo]
            FROM ITEMS_ADQUISICION 
            INNER JOIN PRODUCTOS ON ITEMS_ADQUISICION.ID_PRODUCTO = PRODUCTOS.ID
            WHERE ID_ADQUISICION = " & idProducto

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function CrearItemAdquisicion(itemAdquisicion As ItemAdquisicion)
        Dim sql As String =
            "INSERT INTO ITEMS_ADQUISICION (ID_PRODUCTO, ID_ADQUISICION,  CANTIDAD, COSTO) VALUES (" &
            itemAdquisicion.idProducto & ", " & itemAdquisicion.idAdquisicion & "," & itemAdquisicion.cantidad & ", " &
            itemAdquisicion.costo & ")"

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EditarItemAdquisicion(itemAdquisicion As ItemAdquisicion)
        Dim sql As String =
            "UPDATE ITEMS_ADQUISICION SET ID_ADQUISICION = " & itemAdquisicion.idAdquisicion &
            ", CANTIDAD = " & itemAdquisicion.cantidad & ", COSTO = " & itemAdquisicion.costo &
            "WHERE ID = " & itemAdquisicion.id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EliminarItemAdquisicion(id As String)
        Dim sql As String = "DELETE FROM ITEMS_ADQUISICION WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function
End Class
