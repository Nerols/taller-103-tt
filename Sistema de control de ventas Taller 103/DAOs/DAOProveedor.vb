﻿Public Class DAOProveedor
    Dim conexion As New Conexion

    Public Function GetProveedores()
        Dim sql As String = "
            SELECT
            ID,
            RUT AS [Rut],
            NOMBRE AS [Nombre proveedor],
            DESCRIPCION AS [Descripcion],
            FONO AS [Fono],
            EMAIL AS [Email],
            SITIO AS [Sitio web],
            BORRADO_EN AS [Fecha Borrado]
            FROM PROVEEDORES       
            WHERE BORRADO_EN IS NULL"

        Return conexion.ConsultaSQL(sql)
    End Function
    Public Function GetAllProveedores()
        Dim sql As String = "
            SELECT
            ID,
            RUT AS [Rut],
            NOMBRE AS [Nombre proveedor],
            DESCRIPCION AS [Descripcion],
            FONO AS [Fono],
            EMAIL AS [Email],
            SITIO AS [Sitio web],
            BORRADO_EN AS [Fecha Borrado]
            FROM PROVEEDORES"

        Return conexion.ConsultaSQL(sql)
    End Function
    Public Function GetProveedor(id As String)
        Dim sql As String =
            "SELECT * FROM PROVEEDORES WHERE ID = " & id

        Return conexion.ConsultaSQL(sql).Rows(0)
    End Function

    Public Function BusquedaProveedorRut(busqueda As String)
        Dim sql As String = "
            SELECT 
            ID,
            RUT AS [Rut],
            NOMBRE AS [Nombre proveedor],
            DESCRIPCION AS [Descripcion],
            FONO AS [Fono],
            EMAIL AS [Email],
            SITIO AS [Sitio web],
            BORRADO_EN AS [Fecha Borrado]
            FROM PROVEEDORES    
            WHERE BORRADO_EN IS NULL AND RUT LIKE '" & busqueda & "%'"
        Return conexion.ConsultaSQL(sql)
    End Function
    Public Function BusquedaAllProveedorRut(busqueda As String)
        Dim sql As String = "
            SELECT 
            ID,
            RUT AS [Rut],
            NOMBRE AS [Nombre proveedor],
            DESCRIPCION AS [Descripcion],
            FONO AS [Fono],
            EMAIL AS [Email],
            SITIO AS [Sitio web],
            BORRADO_EN AS [Fecha Borrado]
            FROM PROVEEDORES    
            WHERE RUT LIKE '" & busqueda & "%'"
        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function BusquedaProveedorNombre(busqueda As String)
        Dim sql As String = "
            SELECT 
            ID,
            RUT AS [Rut],
            NOMBRE AS [Nombre proveedor],
            DESCRIPCION AS [Descripcion],
            FONO AS [Fono],
            EMAIL AS [Email],
            SITIO AS [Sitio web],
            BORRADO_EN AS [Fecha Borrado]
            FROM PROVEEDORES    
            WHERE BORRADO_EN IS NULL AND NOMBRE LIKE '" & busqueda & "%'"
        Return conexion.ConsultaSQL(sql)
    End Function
    Public Function BusquedaAllProveedorNombre(busqueda As String)
        Dim sql As String = "
            SELECT 
            ID,
            RUT AS [Rut],
            NOMBRE AS [Nombre proveedor],
            DESCRIPCION AS [Descripcion],
            FONO AS [Fono],
            EMAIL AS [Email],
            SITIO AS [Sitio web],
            BORRADO_EN AS [Fecha Borrado]
            FROM PROVEEDORES    
            WHERE NOMBRE LIKE '" & busqueda & "%'"
        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function CrearProveedor(proveedor As Proveedor)
        Dim sql As String =
            "INSERT INTO PROVEEDORES (RUT, NOMBRE, DESCRIPCION, FONO, EMAIL, SITIO) VALUES ('" & proveedor.rut & "', '" &
            proveedor.nombre & "', '" & proveedor.descripcion & "', '" & proveedor.fono & "', '" &
            proveedor.email & "', '" & proveedor.sitio & "')"

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EditarProveedor(proveedor As Proveedor)
        Dim sql As String =
            "UPDATE PROVEEDORES SET RUT = '" & proveedor.rut & "', NOMBRE = '" & proveedor.nombre & "', DESCRIPCION = '" &
            proveedor.descripcion & "', FONO = '" & proveedor.fono & "', EMAIL = '" &
            proveedor.email & "', SITIO = '" & proveedor.sitio & "' WHERE ID = " & proveedor.id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EliminarProveedor(id As String)
        Dim sql As String =
            "UPDATE PROVEEDORES SET BORRADO_EN = '" & DateTime.Now.ToString("yyyy-MM-dd") & "' WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function RestaurarProveedor(id As String)
        Dim sql As String =
           "UPDATE PROVEEDORES SET BORRADO_EN = NULL WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function
End Class
