﻿Public Class DAOTipo
    Dim conexion As New Conexion

    Public Function GetTipos()
        Dim sql As String = "
            SELECT 
            ID,
            NOMBRE AS [Nombre],
            BORRADO_EN AS [Fecha Borrado]
            FROM TIPOS
            WHERE BORRADO_EN IS NULL
            "

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetAllTipos()
        Dim sql As String = "
            SELECT 
            ID,
            NOMBRE AS [Nombre],
            BORRADO_EN AS [Fecha Borrado]
            FROM TIPOS
            "

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetTipo(id As String)
        Dim sql As String = "SELECT * FROM TIPOS WHERE ID = " & id

        Return conexion.ConsultaSQL(sql).Rows(0)
    End Function

    Public Function BusquedaTipo(busqueda As String)
        Dim sql As String = "
            SELECT * FROM TIPOS 
            WHERE BORRADO_EN IS NULL AND NOMBRE LIKE '" & busqueda & "%'"
        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function BusquedaAllTipo(busqueda As String)
        Dim sql As String = "
            SELECT * FROM TIPOS 
            WHERE NOMBRE LIKE '" & busqueda & "%'"
        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function CrearTipo(tipo As Tipo)
        Dim sql As String =
            "INSERT INTO TIPOS (NOMBRE) VALUES ('" & tipo.nombre & "')"

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EditarTipo(tipo As Tipo)
        Dim sql As String =
            "UPDATE TIPOS SET NOMBRE = '" & tipo.nombre & "' WHERE ID = " & tipo.id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EliminarTipo(id As String)
        Dim sql As String =
            "UPDATE TIPOS SET BORRADO_EN = '" & DateTime.Now.ToString("yyyy-MM-dd") & "' WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function RestaurarTipo(id As String)
        Dim sql As String =
           "UPDATE TIPOS SET BORRADO_EN = NULL WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function
End Class
