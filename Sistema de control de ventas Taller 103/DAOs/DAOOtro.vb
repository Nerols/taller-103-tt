﻿Public Class DAOOtro
    Dim conexion As New Conexion

    Public Function GetOtros()
        Dim sql As String = "
            SELECT
            ID,
            ID_VENTA AS [Número Venta],
            DESCRIPCION AS [Descripción],
            PRECIO AS [Monto Asociado]
            FROM OTROS"

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetOtro(id As String)
        Dim sql As String = "SELECT * FROM OTROS WHERE ID = " & id

        Return conexion.ConsultaSQL(sql).Rows(0)
    End Function

    Public Function BusquedaOtro(busqueda As String)
        Return True
    End Function

    Public Function GetOtrosDeVenta(idVenta As String)
        Dim sql As String = "SELECT * FROM OTROS WHERE ID_VENTA = " & idVenta

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function CrearOtro(otro As Otro)
        Dim sql As String =
            "INSERT INTO OTROS (ID_VENTA, DESCRIPCION, PRECIO) VALUES (" & otro.idVenta & ", '" &
            otro.descripcion & "', " & otro.precio & ")"

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EditarOtro(otro As Otro)
        Dim sql As String =
            "UPDATE OTROS SET ID_VENTA = " & otro.idVenta & ", DESCRIPCION = '" &
            otro.descripcion & "', PRECIO = " & otro.precio & " WHERE ID = " & otro.id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EliminarOtro(id As String)
        Dim sql As String =
            "DELETE FROM OTROS WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function
End Class
