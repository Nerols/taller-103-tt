﻿Public Class DAOAdquisicion
    Dim conexion As New Conexion

    Public Function GetAdquisiciones()
        Dim sql As String = "
            SELECT 
            ADQUISICIONES.ID,
            PROVEEDORES.NOMBRE AS [Nombre Proveedor],
            ADQUISICIONES.FECHA_ADQUISICION AS [Fecha],
            ADQUISICIONES.BORRADO_EN AS [Fecha borrado]
            FROM ADQUISICIONES
            INNER JOIN PROVEEDORES ON ADQUISICIONES.ID_PROVEEDOR = PROVEEDORES.ID
            WHERE ADQUISICIONES.BORRADO_EN IS NULL"

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetAllAdquisiciones()
        Dim sql As String = "
            SELECT 
            ADQUISICIONES.ID,
            PROVEEDORES.NOMBRE AS [Nombre Proveedor],
            ADQUISICIONES.FECHA_ADQUISICION AS [Fecha],
            ADQUISICIONES.BORRADO_EN AS [Fecha borrado]
            FROM ADQUISICIONES
            INNER JOIN PROVEEDORES ON ADQUISICIONES.ID_PROVEEDOR = PROVEEDORES.ID"

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function GetAdquisicion(id As String)
        Dim sql As String = "SELECT * FROM ADQUISICIONES WHERE ID = " & id

        Return conexion.ConsultaSQL(sql).Rows(0)
    End Function

    Public Function BusquedaAdquisicion(busqueda As String)
        Dim sql As String = "
            SELECT 
            ADQUISICIONES.ID,
            PROVEEDORES.NOMBRE,
            ADQUISICIONES.FECHA_ADQUISICION
            FROM ADQUISICIONES
            INNER JOIN PROVEEDORES ON ADQUISICIONES.ID_PROVEEDOR = PROVEEDORES.ID
            WHERE ADQUISICIONES.BORRADO_EN IS NULL AND PROVEEDORES.NOMBRE LIKE '" & busqueda & "%'"

        Return conexion.ConsultaSQL(sql)
    End Function

    Public Function BusquedaAllAdquisicion(busqueda As String)
        Dim sql As String = "
            SELECT 
            ADQUISICIONES.ID,
            PROVEEDORES.NOMBRE,
            ADQUISICIONES.FECHA_ADQUISICION
            FROM ADQUISICIONES
            INNER JOIN PROVEEDORES ON ADQUISICIONES.ID_PROVEEDOR = PROVEEDORES.ID
            WHERE PROVEEDORES.NOMBRE LIKE '" & busqueda & "%'"

        Return conexion.ConsultaSQL(sql)
    End Function
    Public Function GetAdquisicionMax()
        Dim sql As String = "SELECT MAX(ID) FROM ADQUISICIONES"

        Return conexion.ConsultaSQL(sql).Rows(0)
    End Function

    Public Function CrearAdquisicion(adquisicion As Adquisicion)
        Dim sql As String =
            "INSERT INTO ADQUISICIONES 
            (ID_PROVEEDOR, FECHA_ADQUISICION) 
            VALUES (" & adquisicion.idProveedor & ", '" & adquisicion.fechaAdquisicion.ToString("yyyy-MM-dd") & "')"

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EditarAdquisicion(adquisicion As Adquisicion)
        Dim sql As String =
            "UPDATE ADQUISICIONES SET ID_PROVEEDOR = " & adquisicion.idProveedor & ", FECHA_ADQUISICION = '" &
            adquisicion.fechaAdquisicion & "' WHERE ID = " & adquisicion.id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function EliminarAdquisicion(id As String)
        Dim sql As String = "UPDATE ADQUISICIONES SET BORRADO_EN = '" & DateTime.Now.ToString("yyyy-MM-dd") & "' WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function

    Public Function RestaurarAdquisicion(id As String)
        Dim sql As String = "UPDATE ADQUISICIONES SET BORRADO_EN = NULL WHERE ID = " & id

        Return conexion.ComandoSQL(sql)
    End Function
End Class
