﻿Public Class BSNItemVenta
    Dim daoItemVenta As New DAOItemVenta

    Public Function GetItemVentaDeProducto(idProducto As String)
        Return daoItemVenta.GetItemVentaDeProducto(idProducto)
    End Function

    Public Function GetItemsVentaDeVenta(idVenta As String)
        Return daoItemVenta.GetItemsVentaDeVenta(idVenta)
    End Function

    Public Function GetItemsVentaDeVentaLimpio(idVenta As String)
        Return daoItemVenta.GetItemsVentaDeVentaLimpio(idVenta)
    End Function

    Public Function CrearItemVenta(itemVenta As ItemVenta)
        Return daoItemVenta.CrearItemVenta(itemVenta)
    End Function

    Public Function EditarItemVenta(itemVenta As ItemVenta)
        Return daoItemVenta.EditarItemVenta(itemVenta)
    End Function

    Public Function EliminarItemVenta(id As String)
        Return daoItemVenta.EliminarItemVenta(id)
    End Function
End Class
