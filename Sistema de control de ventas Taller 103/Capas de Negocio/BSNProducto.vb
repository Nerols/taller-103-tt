﻿Imports System.Text.RegularExpressions

Public Class BSNProducto
    Dim daoProducto As New DAOProducto

    Public Function GetProductos()
        Return daoProducto.GetProductos()
    End Function

    Public Function GetProducto(id As String)
        Return daoProducto.GetProducto(id)
    End Function

    Public Function GetAllProductos()
        Return daoProducto.GetAllProductos()
    End Function

    Public Function BusquedaProducto(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoProducto.BusquedaProducto(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function BusquedaAllProducto(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoProducto.BusquedaAllProducto(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function CrearProducto(producto As Producto)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        producto.nombre = rgxAlphaNum.Replace(producto.nombre, "")

        If (producto.nombre = "") Then
            Return False
        Else
            Return daoProducto.CrearProducto(producto)
        End If
    End Function

    Public Function EditarProducto(producto As Producto)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        producto.nombre = rgxAlphaNum.Replace(producto.nombre, "")

        If (producto.nombre = "") Then
            Return False
        Else
            Return daoProducto.EditarProducto(producto)
        End If
    End Function

    Public Function EliminarProducto(id As String)
        Return daoProducto.EliminarProducto(id)
    End Function

    Public Function RestaurarProducto(id As String)
        Return daoProducto.RestaturarProducto(id)
    End Function
End Class
