﻿Imports System.Text.RegularExpressions

Public Class BSNTipo
    Dim daoTipo As New DAOTipo

    Public Function GetTipos()
        Return daoTipo.GetTipos()
    End Function

    Public Function GetAllTipos()
        Return daoTipo.GetAllTipos()
    End Function

    Public Function GetTipo(id As String)
        Return daoTipo.GetTipo(id)
    End Function
    Public Function BusquedaTipo(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoTipo.BusquedaTipo(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function BusquedaAllTipo(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoTipo.BusquedaAllTipo(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function CrearTipo(tipo As Tipo)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        tipo.nombre = rgxAlphaNum.Replace(tipo.nombre, "")

        If (tipo.nombre = "") Then
            Return False
        Else
            Return daoTipo.CrearTipo(tipo)
        End If
    End Function

    Public Function EditarTipo(tipo As Tipo)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        tipo.nombre = rgxAlphaNum.Replace(tipo.nombre, "")

        If (tipo.nombre = "") Then
            Return False
        Else
            Return daoTipo.EditarTipo(tipo)
        End If
    End Function

    Public Function EliminarTipo(id As String)
        Return daoTipo.EliminarTipo(id)
    End Function

    Public Function RestaurarTipo(id As String)
        Return daoTipo.RestaurarTipo(id)
    End Function
End Class
