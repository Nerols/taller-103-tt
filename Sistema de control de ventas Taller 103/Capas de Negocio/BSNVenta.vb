﻿Imports System.Text.RegularExpressions

Public Class BSNVenta
    Dim daoVenta As New DAOVenta

    Public Function GetVentas()
        Return daoVenta.GetVentas()
    End Function

    Public Function GetAllVentas()
        Return daoVenta.GetAllVentas()
    End Function

    Public Function GetVenta(id As String)
        Return daoVenta.GetVenta(id)
    End Function

    Public Function BusquedaVenta(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoVenta.BusquedaVenta(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function BusquedaAllVenta(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoVenta.BusquedaAllVenta(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function GetUltVenta()
        Return daoVenta.GetUltVenta()
    End Function

    Public Function CrearVenta(venta As Venta)
        Return daoVenta.CrearVenta(venta)
    End Function

    Public Function EstablecerFechaEntrega(venta As Venta)
        Return daoVenta.EstablecerFechaEntrega(venta)
    End Function

    Public Function EliminarVenta(id As String)
        Return daoVenta.EliminarVenta(id)
    End Function
    Public Function RestaurarVenta(id As String)
        Return daoVenta.RestaurarVenta(id)
    End Function
End Class
