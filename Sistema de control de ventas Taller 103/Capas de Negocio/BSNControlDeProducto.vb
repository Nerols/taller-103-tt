﻿Imports System.Text.RegularExpressions
Public Class BSNControlDeProducto
    Dim daoControlProducto As New DAOControlDeProducto

    Public Function GetControlesDeProductos()
        Return daoControlProducto.GetControlesDeProductos()
    End Function

    Public Function GetControlDeProducto(id As String)
        Return daoControlProducto.GetControlDeProducto(id)
    End Function

    Public Function GetControlesDeProducto(idProducto As String)
        Return daoControlProducto.GetControlesDeProducto(idProducto)
    End Function

    Public Function BusquedaControlDeProducto(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoControlProducto.BusquedaControlDeProducto(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function CrearControlDeProducto(controlProducto As ControlDeProducto)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")
        controlProducto.descripcion = rgxAlphaNum.Replace(controlProducto.descripcion, "")


        If (controlProducto.descripcion = "") Then
            Return False
        Else
            Return daoControlProducto.CrearControlDeProducto(controlProducto)
        End If

    End Function

    Public Function EditarControlDeProducto(controlProducto As ControlDeProducto)
        Return daoControlProducto.EditarControlDeProducto(controlProducto)
    End Function

    Public Function EliminadControlDeProducto(id As String)
        Return daoControlProducto.EliminarControlDeProducto(id)
    End Function
End Class
