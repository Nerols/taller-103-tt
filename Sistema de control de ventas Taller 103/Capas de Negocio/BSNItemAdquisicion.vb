﻿Public Class BSNItemAdquisicion
    Dim daoItemAdquisicion As New DAOItemAdquisicion

    Public Function GetItemAdquisicionDeProducto(idProducto As String)
        Return daoItemAdquisicion.GetItemAdquisicionDeProducto(idProducto)
    End Function

    Public Function CrearItemAdquisicion(itemAdquisicion As ItemAdquisicion)
        Return daoItemAdquisicion.CrearItemAdquisicion(itemAdquisicion)
    End Function

    Public Function EditarItemAdquisicion(itemAdquisicion As ItemAdquisicion)
        Return daoItemAdquisicion.EditarItemAdquisicion(itemAdquisicion)
    End Function

    Public Function EliminarItemAdquisicion(id As String)
        Return daoItemAdquisicion.EliminarItemAdquisicion(id)
    End Function
End Class
