﻿Imports System.Text.RegularExpressions

Public Class BSNOtro
    Dim daoOtro As New DAOOtro

    Public Function GetOtros()
        Return daoOtro.GetOtros()
    End Function

    Public Function GetOtro(id As String)
        Return daoOtro.GetOtro(id)
    End Function

    Public Function BusquedaProducto(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoOtro.BusquedaOtro(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function CrearOtro(otro As Otro)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        otro.descripcion = rgxAlphaNum.Replace(otro.descripcion, "")

        If (otro.descripcion = "") Then
            Return False
        Else
            Return daoOtro.CrearOtro(otro)
        End If

        Return daoOtro.CrearOtro(otro)
    End Function

    Public Function EditarOtro(otro As Otro)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        otro.descripcion = rgxAlphaNum.Replace(otro.descripcion, "")

        If (otro.descripcion = "") Then
            Return False
        Else
            Return daoOtro.EditarOtro(otro)
        End If

        Return daoOtro.EditarOtro(otro)
    End Function

    Public Function EliminarOtro(id As String)
        Return daoOtro.EliminarOtro(id)
    End Function
End Class
