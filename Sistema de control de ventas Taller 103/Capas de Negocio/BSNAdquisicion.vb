﻿Imports System.Text.RegularExpressions

Public Class BSNAdquisicion
    Dim daoAdquisicion As New DAOAdquisicion
    Public Function GetAdquisiciones()
        Return daoAdquisicion.GetAdquisiciones()
    End Function

    Public Function GetAllAdquisiciones()
        Return daoAdquisicion.GetAllAdquisiciones()
    End Function

    Public Function GetAdquisicion(id As String)
        Return daoAdquisicion.GetAdquisicion(id)
    End Function

    Public Function GetAdquisicionMax()
        Return daoAdquisicion.GetAdquisicionMax()
    End Function

    Public Function BusquedaAdquisicion(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoAdquisicion.BusquedaAdquisicion(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function BusquedaAllAdquisicion(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoAdquisicion.BusquedaAllAdquisicion(rgxAlphaNum.Replace(busqueda, ""))
    End Function
    Public Function CrearAdquisicion(adquisicion As Adquisicion)
        Return daoAdquisicion.CrearAdquisicion(adquisicion)
    End Function

    Public Function EditarAdquisicion(adquisicion As Adquisicion)
        Return daoAdquisicion.EditarAdquisicion(adquisicion)
    End Function

    Public Function EliminarAdquisicion(id As String)
        Return daoAdquisicion.EliminarAdquisicion(id)
    End Function

    Public Function RestaurarAdquisicion(id As String)
        Return daoAdquisicion.RestaurarAdquisicion(id)
    End Function
End Class
