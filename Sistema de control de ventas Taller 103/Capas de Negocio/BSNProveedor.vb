﻿Imports System.Text.RegularExpressions

Public Class BSNProveedor
    Dim daoProveedor As New DAOProveedor

    Public Function GetProveedores()
        Return daoProveedor.GetProveedores()
    End Function

    Public Function GetAllProveedores()
        Return daoProveedor.GetAllProveedores()
    End Function

    Public Function GetProveedor(id As String)
        Return daoProveedor.GetProveedor(id)
    End Function

    Public Function BusquedaProveedorRut(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoProveedor.BusquedaProveedorRut(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function BusquedaAllProveedorRut(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoProveedor.BusquedaAllProveedorRut(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function BusquedaProveedorNombre(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoProveedor.BusquedaProveedorNombre(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function BusquedaAllProveedorNombre(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoProveedor.BusquedaAllProveedorNombre(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function CrearProveedor(proveedor As Proveedor)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        proveedor.nombre = rgxAlphaNum.Replace(proveedor.nombre, "")
        proveedor.descripcion = rgxAlphaNum.Replace(proveedor.descripcion, "")
        proveedor.fono = rgxAlphaNum.Replace(proveedor.fono, "")
        proveedor.email = rgxAlphaNum.Replace(proveedor.email, "")
        proveedor.sitio = rgxAlphaNum.Replace(proveedor.sitio, "")

        If (proveedor.nombre = "" Or proveedor.descripcion = "" Or proveedor.fono = "" Or proveedor.email = "" Or proveedor.sitio = "") Then
            Return False
        Else
            Return daoProveedor.CrearProveedor(proveedor)
        End If
    End Function

    Public Function EditarProveedor(proveedor As Proveedor)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        proveedor.nombre = rgxAlphaNum.Replace(proveedor.nombre, "")
        proveedor.descripcion = rgxAlphaNum.Replace(proveedor.descripcion, "")
        proveedor.fono = rgxAlphaNum.Replace(proveedor.fono, "")
        proveedor.email = rgxAlphaNum.Replace(proveedor.email, "")
        proveedor.sitio = rgxAlphaNum.Replace(proveedor.sitio, "")

        If (proveedor.nombre = "" Or proveedor.descripcion = "" Or proveedor.fono = "" Or proveedor.email = "" Or proveedor.sitio = "") Then
            Return False
        Else
            Return daoProveedor.EditarProveedor(proveedor)
        End If
    End Function

    Public Function EliminarProveedor(id As String)
        Return daoProveedor.EliminarProveedor(id)
    End Function

    Public Function RestaurarProovedor(id As String)
        Return daoProveedor.RestaurarProveedor(id)
    End Function
End Class
