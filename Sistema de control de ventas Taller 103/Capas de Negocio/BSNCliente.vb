﻿Imports System.Text.RegularExpressions
Public Class BSNCliente
    Dim daoCliente As New DAOCliente

    Public Function GetClientes()
        Return daoCliente.GetClientes()
    End Function

    Public Function GetAllClientes()
        Return daoCliente.GetAllClientes()
    End Function

    Public Function GetCliente(id As String)
        Return daoCliente.GetCliente(id)
    End Function

    Public Function GetClientePorRut(rut As String)
        Dim rgx As New Regex("[^\w\s\-]")

        Return daoCliente.GetClientePorRut(rgx.Replace(rut, ""))
    End Function

    Public Function BusquedaClienteRut(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoCliente.BusquedaClienteRut(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function BusquedaAllClienteRut(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoCliente.BusquedaAllClienteRut(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function BusquedaClienteNombre(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoCliente.BusquedaClienteNombre(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function BusquedaAllClienteNombre(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoCliente.BusquedaAllClienteNombre(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function CrearCliente(cliente As Cliente)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        cliente.nombre = rgxAlphaNum.Replace(cliente.nombre, "")
        cliente.apellido = rgxAlphaNum.Replace(cliente.apellido, "")
        cliente.fono1 = rgxAlphaNum.Replace(cliente.fono1, "")
        cliente.fono2 = rgxAlphaNum.Replace(cliente.fono2, "")
        cliente.email = rgxAlphaNum.Replace(cliente.email, "")
        cliente.direccion = rgxAlphaNum.Replace(cliente.direccion, "")

        If (cliente.nombre = "" Or cliente.apellido = "" Or cliente.fono1 = "" Or cliente.email = "" Or cliente.direccion = "") Then
            Return Nothing
        Else
            Return daoCliente.CrearCliente(cliente)
        End If
    End Function

    Public Function EditarCliente(cliente As Cliente)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        cliente.nombre = rgxAlphaNum.Replace(cliente.nombre, "")
        cliente.apellido = rgxAlphaNum.Replace(cliente.apellido, "")
        cliente.fono1 = rgxAlphaNum.Replace(cliente.fono1, "")
        cliente.fono2 = rgxAlphaNum.Replace(cliente.fono2, "")
        cliente.email = rgxAlphaNum.Replace(cliente.email, "")
        cliente.direccion = rgxAlphaNum.Replace(cliente.direccion, "")

        If (cliente.nombre = "" Or cliente.apellido = "" Or cliente.fono1 = "" Or cliente.email = "" Or cliente.direccion = "") Then
            Return False
        Else
            Return daoCliente.EditarCliente(cliente)
        End If
    End Function

    Public Function EliminarCliente(id As String)
        Return daoCliente.EliminarCliente(id)
    End Function

    Public Function RestaurarCliente(id As String)
        Return daoCliente.RestaurarCliente(id)
    End Function
End Class
