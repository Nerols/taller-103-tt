﻿Imports System.Text.RegularExpressions

Public Class BSNEnvio
    Dim daoEnvio As New DAOEnvio

    Public Function GetEnvios()
        Return daoEnvio.GetEnvios()
    End Function

    Public Function GetEnvio(id As String)
        Return daoEnvio.GetEnvio(id)
    End Function

    Public Function BusquedaEnvio(busqueda As String)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        Return daoEnvio.BusquedaEnvio(rgxAlphaNum.Replace(busqueda, ""))
    End Function

    Public Function CrearEnvio(envio As Envio)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        envio.numeroEnvio = rgxAlphaNum.Replace(envio.numeroEnvio, "")
        envio.descripcion = rgxAlphaNum.Replace(envio.descripcion, "")
        envio.courier = rgxAlphaNum.Replace(envio.courier, "")

        If (envio.descripcion = "" Or envio.numeroEnvio = "" Or envio.courier = "") Then
            Return False
        Else
            Return daoEnvio.CrearEnvio(envio)
        End If
    End Function

    Public Function EditarEnvio(envio As Envio)
        Dim rgxAlphaNum As New Regex("[^\w\s\""\.\,\@]")

        envio.numeroEnvio = rgxAlphaNum.Replace(envio.numeroEnvio, "")
        envio.descripcion = rgxAlphaNum.Replace(envio.descripcion, "")
        envio.courier = rgxAlphaNum.Replace(envio.courier, "")

        If (envio.descripcion = "" Or envio.numeroEnvio = "" Or envio.courier = "") Then
            Return False
        Else
            Return daoEnvio.EditarEnvio(envio)
        End If
    End Function

    Public Function EliminarEnvio(id As String)
        Return daoEnvio.EliminarEnvio(id)
    End Function
End Class
