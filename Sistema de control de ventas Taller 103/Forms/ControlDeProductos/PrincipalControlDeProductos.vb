﻿Public Class PrincipalControlDeProductos
    Dim bsnControlDeProductos As New BSNControlDeProducto
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        CrearControlDeProductos.ShowDialog()
    End Sub

    Private Sub BtnEditar_Click(sender As Object, e As EventArgs)
        EditarControlDeProductos.ShowDialog()
    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub PrincipalControlDeProductos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvControlDeProductos.DataSource = bsnControlDeProductos.GetControlesDeProductos()
        dgvControlDeProductos.Columns.Item(0).Visible = False
    End Sub

    Private Sub TxtBusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtBusqueda.TextChanged
        dgvControlDeProductos.DataSource = bsnControlDeProductos.BusquedaControlDeProducto(txtBusqueda.Text)
    End Sub
End Class