﻿Public Class CrearControlDeProductos
    Dim bsnProducto As New BSNProducto
    Dim bsnControlDeProducto As New BSNControlDeProducto
    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub CrearControlDeProductos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbProducto.DataSource = bsnProducto.GetProductos()
        cbProducto.ValueMember = "ID"
        cbProducto.DisplayMember = "NOMBRE"
        cbTipo.Items.Add("Pérdida")
        cbTipo.Items.Add("Ganancia")
    End Sub

    Private Sub BtnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click


        If (cbTipo.SelectedIndex = -1 Or Trim(txtDescripcion.Text) = "" Or Trim(txtCantidad.Text = "") Or cbProducto.SelectedIndex = -1) Then
            MessageBox.Show("Verifique los datos e inténtelo nuevamente", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If


        Dim controlProductos As New ControlDeProducto
        Dim producto As New Producto

        Dim dataSet = bsnProducto.GetProducto(cbProducto.SelectedValue)
        producto.id = dataSet.Item(0).ToString()
        producto.idTipo = dataSet.Item(1).ToString()
        producto.nombre = dataSet.Item(2).ToString()
        producto.precio = dataSet.Item(3).ToString()
        producto.costo = dataSet.Item(4).ToString()
        producto.stock = dataSet.Item(5).ToString()
        producto.stockComp = dataSet.Item(6).ToString()

        If (producto.stock - txtCantidad.Text) < 0 And cbTipo.SelectedIndex = 0 Then
            MessageBox.Show("No es posible ingresar perdida, stock menor a 0", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If (cbTipo.SelectedIndex = 0) Then
            producto.stock = producto.stock - txtCantidad.Text
        Else
            producto.stock = producto.stock + txtCantidad.Text
        End If
        Dim dateString As String
        dateString = dtpFecha.Value.ToString("dd-MM-yyyy")
        controlProductos.idProducto = cbProducto.SelectedValue
        controlProductos.fecha = dateString
        controlProductos.tipo = cbTipo.SelectedItem
        controlProductos.descripcion = txtDescripcion.Text
        controlProductos.cantidad = txtCantidad.Text

        If MessageBox.Show("¿Está seguro de realizar este control de producto?" & Chr(13) & "Tipo: " & cbTipo.SelectedItem & Chr(13) & "Cantidad: " & txtCantidad.Text & Chr(13), "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
            bsnControlDeProducto.CrearControlDeProducto(controlProductos)
            bsnProducto.EditarProducto(producto)
            MessageBox.Show("Control de producto ingresado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
            PrincipalControlDeProductos.dgvControlDeProductos.DataSource = bsnControlDeProducto.GetControlesDeProductos()
            Dispose()
        End If
    End Sub

    Private Sub TxtCantidad_Keypress(sender As Object, e As KeyPressEventArgs) Handles txtCantidad.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub
End Class