﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MenuPrincipal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MenuPrincipal))
        Me.btnAdquisicion = New System.Windows.Forms.Button()
        Me.btnProductos = New System.Windows.Forms.Button()
        Me.btnClientes = New System.Windows.Forms.Button()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnOtros = New System.Windows.Forms.Button()
        Me.btnControlProductos = New System.Windows.Forms.Button()
        Me.btnEnvios = New System.Windows.Forms.Button()
        Me.btnTiposProducto = New System.Windows.Forms.Button()
        Me.btnProveedores = New System.Windows.Forms.Button()
        Me.btnVentas = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnAdquisicion
        '
        Me.btnAdquisicion.Image = Global.Sistema_de_control_de_ventas_Taller_103.My.Resources.Resources.box_full
        Me.btnAdquisicion.Location = New System.Drawing.Point(310, 164)
        Me.btnAdquisicion.Name = "btnAdquisicion"
        Me.btnAdquisicion.Size = New System.Drawing.Size(130, 62)
        Me.btnAdquisicion.TabIndex = 8
        Me.btnAdquisicion.Text = "Adquisiciones"
        Me.btnAdquisicion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAdquisicion.UseVisualStyleBackColor = True
        '
        'btnProductos
        '
        Me.btnProductos.Image = Global.Sistema_de_control_de_ventas_Taller_103.My.Resources.Resources.Bookmark48
        Me.btnProductos.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnProductos.Location = New System.Drawing.Point(310, 28)
        Me.btnProductos.Name = "btnProductos"
        Me.btnProductos.Size = New System.Drawing.Size(130, 62)
        Me.btnProductos.TabIndex = 2
        Me.btnProductos.Text = "Productos"
        Me.btnProductos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnProductos.UseVisualStyleBackColor = True
        '
        'btnClientes
        '
        Me.btnClientes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnClientes.Image = Global.Sistema_de_control_de_ventas_Taller_103.My.Resources.Resources.profile_group
        Me.btnClientes.Location = New System.Drawing.Point(38, 28)
        Me.btnClientes.Name = "btnClientes"
        Me.btnClientes.Size = New System.Drawing.Size(130, 62)
        Me.btnClientes.TabIndex = 0
        Me.btnClientes.Text = "Clientes"
        Me.btnClientes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClientes.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Image = Global.Sistema_de_control_de_ventas_Taller_103.My.Resources.Resources.sign_error
        Me.btnSalir.Location = New System.Drawing.Point(174, 232)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(130, 62)
        Me.btnSalir.TabIndex = 9
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnOtros
        '
        Me.btnOtros.Image = Global.Sistema_de_control_de_ventas_Taller_103.My.Resources.Resources.file_empty
        Me.btnOtros.Location = New System.Drawing.Point(38, 96)
        Me.btnOtros.Name = "btnOtros"
        Me.btnOtros.Size = New System.Drawing.Size(130, 62)
        Me.btnOtros.TabIndex = 3
        Me.btnOtros.Text = "Otros"
        Me.btnOtros.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnOtros.UseVisualStyleBackColor = True
        '
        'btnControlProductos
        '
        Me.btnControlProductos.Image = Global.Sistema_de_control_de_ventas_Taller_103.My.Resources.Resources.cogs
        Me.btnControlProductos.Location = New System.Drawing.Point(310, 96)
        Me.btnControlProductos.Name = "btnControlProductos"
        Me.btnControlProductos.Size = New System.Drawing.Size(130, 62)
        Me.btnControlProductos.TabIndex = 5
        Me.btnControlProductos.Text = "Control de productos"
        Me.btnControlProductos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnControlProductos.UseVisualStyleBackColor = True
        '
        'btnEnvios
        '
        Me.btnEnvios.Image = Global.Sistema_de_control_de_ventas_Taller_103.My.Resources.Resources.envelope
        Me.btnEnvios.Location = New System.Drawing.Point(174, 164)
        Me.btnEnvios.Name = "btnEnvios"
        Me.btnEnvios.Size = New System.Drawing.Size(130, 62)
        Me.btnEnvios.TabIndex = 7
        Me.btnEnvios.Text = "Envíos"
        Me.btnEnvios.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnEnvios.UseVisualStyleBackColor = True
        '
        'btnTiposProducto
        '
        Me.btnTiposProducto.Image = CType(resources.GetObject("btnTiposProducto.Image"), System.Drawing.Image)
        Me.btnTiposProducto.Location = New System.Drawing.Point(174, 28)
        Me.btnTiposProducto.Name = "btnTiposProducto"
        Me.btnTiposProducto.Size = New System.Drawing.Size(130, 62)
        Me.btnTiposProducto.TabIndex = 1
        Me.btnTiposProducto.Text = "Tipos de producto"
        Me.btnTiposProducto.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnTiposProducto.UseVisualStyleBackColor = True
        '
        'btnProveedores
        '
        Me.btnProveedores.Image = Global.Sistema_de_control_de_ventas_Taller_103.My.Resources.Resources.user_male
        Me.btnProveedores.Location = New System.Drawing.Point(38, 164)
        Me.btnProveedores.Name = "btnProveedores"
        Me.btnProveedores.Size = New System.Drawing.Size(130, 62)
        Me.btnProveedores.TabIndex = 6
        Me.btnProveedores.Text = "Proveedores"
        Me.btnProveedores.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnProveedores.UseVisualStyleBackColor = True
        '
        'btnVentas
        '
        Me.btnVentas.Image = Global.Sistema_de_control_de_ventas_Taller_103.My.Resources.Resources.shop
        Me.btnVentas.Location = New System.Drawing.Point(174, 96)
        Me.btnVentas.Name = "btnVentas"
        Me.btnVentas.Size = New System.Drawing.Size(130, 62)
        Me.btnVentas.TabIndex = 4
        Me.btnVentas.Text = "Ventas"
        Me.btnVentas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnVentas.UseVisualStyleBackColor = True
        '
        'MenuPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(477, 315)
        Me.Controls.Add(Me.btnAdquisicion)
        Me.Controls.Add(Me.btnProductos)
        Me.Controls.Add(Me.btnClientes)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnOtros)
        Me.Controls.Add(Me.btnControlProductos)
        Me.Controls.Add(Me.btnEnvios)
        Me.Controls.Add(Me.btnTiposProducto)
        Me.Controls.Add(Me.btnProveedores)
        Me.Controls.Add(Me.btnVentas)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "MenuPrincipal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Menú Principal - Sistema de control de ventas Taller 103"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnVentas As Button
    Friend WithEvents btnProveedores As Button
    Friend WithEvents btnTiposProducto As Button
    Friend WithEvents btnEnvios As Button
    Friend WithEvents btnControlProductos As Button
    Friend WithEvents btnOtros As Button
    Friend WithEvents btnSalir As Button
    Friend WithEvents btnClientes As Button
    Friend WithEvents btnProductos As Button
    Friend WithEvents btnAdquisicion As Button
End Class
