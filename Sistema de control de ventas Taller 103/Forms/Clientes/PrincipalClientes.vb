﻿Public Class PrincipalClientes
    Dim bsnClientes As New BSNCliente
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        CrearClientes.ShowDialog()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        If dgvClientes.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim cliente As New Cliente
            Dim data = bsnClientes.GetCliente(dgvClientes.SelectedRows.Item(0).Cells(0).Value.ToString)

            If Not DBNull.Value.Equals(data.Item(8)) Then
                MessageBox.Show("No es posible editar un cliente eliminado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            cliente.id = data.item(0)
            cliente.rut = data.item(1)
            cliente.nombre = data.item(2)
            cliente.apellido = data.item(3)
            cliente.fono1 = data.item(4)
            cliente.fono2 = data.item(5)
            cliente.email = data.item(6)
            cliente.direccion = data.item(7)

            Dim frmEditarClientes As New EditarClientes(Me, cliente)

            frmEditarClientes.ShowDialog()
        End If
    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub PrincipalClientes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvClientes.DataSource = bsnClientes.GetClientes()
        dgvClientes.Columns.Item(0).Visible = False
    End Sub

    Private Sub TxtBusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtBusqueda.TextChanged
        If chkBorrados.Checked = True Then
            If rbRut.Checked = True Then
                dgvClientes.DataSource = bsnClientes.BusquedaAllClienteRut(txtBusqueda.Text)
            Else
                dgvClientes.DataSource = bsnClientes.BusquedaAllClienteNombre(txtBusqueda.Text)
            End If
        Else
            If rbRut.Checked = True Then
                dgvClientes.DataSource = bsnClientes.BusquedaClienteRut(txtBusqueda.Text)
            Else
                dgvClientes.DataSource = bsnClientes.BusquedaClienteNombre(txtBusqueda.Text)
            End If
        End If
    End Sub

    Private Sub BtnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If dgvClientes.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim cliente As New Cliente()
            Dim data = bsnClientes.GetCliente(dgvClientes.SelectedRows.Item(0).Cells(0).Value.ToString)
            cliente.id = data.item(0)
            If Not DBNull.Value.Equals(data.Item(8)) Then
                MessageBox.Show("El cliente ya está eliminado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If MessageBox.Show("¿ Está seguro de eliminar este cliente ?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                bsnClientes.EliminarCliente(cliente.id)
                MessageBox.Show("Cliente eliminado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                chkBorrados.Checked = False
                dgvClientes.DataSource = bsnClientes.GetClientes()
            End If
        End If
    End Sub

    Private Sub ChkBorrados_CheckedChanged(sender As Object, e As EventArgs) Handles chkBorrados.CheckedChanged
        If chkBorrados.Checked = True Then
            dgvClientes.DataSource = bsnClientes.GetAllClientes()
        Else
            dgvClientes.DataSource = bsnClientes.GetClientes()
        End If
    End Sub

    Private Sub BtnRestaurar_Click(sender As Object, e As EventArgs) Handles btnRestaurar.Click
        If dgvClientes.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim cliente As New Cliente()
            Dim data = bsnClientes.GetCliente(dgvClientes.SelectedRows.Item(0).Cells(0).Value.ToString)
            cliente.id = data.item(0)
            If DBNull.Value.Equals(data.Item(8)) Then
                MessageBox.Show("No es posible restaurar un cliente que no ha sido eliminado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If MessageBox.Show("¿ Está seguro de restaurar este cliente ?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                bsnClientes.RestaurarCliente(cliente.id)
                MessageBox.Show("Cliente restaurado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                chkBorrados.Checked = False
            End If
        End If
    End Sub
End Class