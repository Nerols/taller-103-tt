﻿Public Class CrearClientes
    Dim bsnCliente As New BSNCliente
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub TxtRut_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRut.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub BtnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        Dim cliente As New Cliente
        'Validación campos vacios
        If txtRut.Text = "" Or txtDigito.Text = "" Or txtNombre.Text = "" Or txtApellido.Text = "" Or txtFono1.Text = "" Or txtEmail.Text = "" Or txtDireccion.Text = "" Then
            MessageBox.Show("Campos en blanco, ingrese datos porfavor", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        'Validación rut menor a 1M
        If txtRut.Text.Length < 7 Then
            MessageBox.Show("Rut demasiado corto, reingrese", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        'Validación de digito
        If txtDigito.Text <> "0" And txtDigito.Text <> "1" And txtDigito.Text <> "2" And txtDigito.Text <> "3" And txtDigito.Text <> "4" And txtDigito.Text <> "5" And txtDigito.Text <> "6" And txtDigito.Text <> "7" And txtDigito.Text <> "8" And txtDigito.Text <> "9" And txtDigito.Text <> "K" Then
            MessageBox.Show("El digito verificador no es valido", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        'Validación de rut
        If txtRut.Text.Length = 8 Then
            Dim total As Integer = 0
            total = total + (txtRut.Text.Substring(7, 1) * 2)
            total = total + (txtRut.Text.Substring(6, 1) * 3)
            total = total + (txtRut.Text.Substring(5, 1) * 4)
            total = total + (txtRut.Text.Substring(4, 1) * 5)
            total = total + (txtRut.Text.Substring(3, 1) * 6)
            total = total + (txtRut.Text.Substring(2, 1) * 7)
            total = total + (txtRut.Text.Substring(1, 1) * 2)
            total = total + (txtRut.Text.Substring(0, 1) * 3)
            Dim nRut As Integer = Math.Truncate(total / 11)
            Dim kRut As String = "A"
            nRut = nRut * 11
            nRut = total - nRut
            nRut = 11 - nRut
            If nRut = 11 Then
                nRut = 0
            End If
            If nRut = 10 Then
                kRut = "K"
            End If
            If nRut.ToString <> txtDigito.Text And kRut <> txtDigito.Text Then
                MessageBox.Show("El rut ingresado no es valido", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
        Else
            Dim total As Integer = 0
            total = total + (txtRut.Text.Substring(6, 1) * 2)
            total = total + (txtRut.Text.Substring(5, 1) * 3)
            total = total + (txtRut.Text.Substring(4, 1) * 4)
            total = total + (txtRut.Text.Substring(3, 1) * 5)
            total = total + (txtRut.Text.Substring(2, 1) * 6)
            total = total + (txtRut.Text.Substring(1, 1) * 7)
            total = total + (txtRut.Text.Substring(0, 1) * 2)
            Dim n As Integer = Math.Truncate(total / 11)
            n = n * 11
            n = total - n
            n = 11 - n
            If n = 11 Then
                n = 0
            End If
            If n = 10 Then
                n = "K"
            End If
            If n.ToString <> txtDigito.Text Then
                MessageBox.Show("El rut ingresado no es valido", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
        End If
        Dim rut As String
        rut = txtRut.Text & "-" & txtDigito.Text
        cliente.rut = rut
        cliente.nombre = txtNombre.Text
        cliente.apellido = txtApellido.Text
        cliente.fono1 = txtFono1.Text
        cliente.fono2 = txtFono2.Text
        cliente.direccion = txtDireccion.Text
        cliente.email = txtEmail.Text
        bsnCliente.CrearCliente(cliente)
        MessageBox.Show("Cliente ingresado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
        PrincipalClientes.dgvClientes.DataSource = bsnCliente.GetClientes()
        Dispose()
    End Sub
End Class
