﻿Public Class MenuPrincipal
    Private Sub BtnProductos_Click(sender As Object, e As EventArgs) Handles btnProductos.Click
        PrincipalProductos.ShowDialog(Me)
    End Sub

    Private Sub BtnVentas_Click(sender As Object, e As EventArgs) Handles btnVentas.Click
        PrincipalVentas.ShowDialog(Me)
    End Sub

    Private Sub BtnProveedores_Click(sender As Object, e As EventArgs) Handles btnProveedores.Click
        PrincipalProveedores.ShowDialog(Me)
    End Sub

    Private Sub BtnClientes_Click(sender As Object, e As EventArgs) Handles btnClientes.Click
        PrincipalClientes.ShowDialog(Me)
    End Sub

    Private Sub BtnTiposProducto_Click(sender As Object, e As EventArgs) Handles btnTiposProducto.Click
        PrincipalTipos.ShowDialog(Me)
    End Sub

    Private Sub BtnEnvios_Click(sender As Object, e As EventArgs) Handles btnEnvios.Click
        PrincipalEnvios.ShowDialog(Me)
    End Sub

    Private Sub BtnOtros_Click(sender As Object, e As EventArgs) Handles btnOtros.Click
        PrincipalOtros.ShowDialog(Me)
    End Sub

    Private Sub BtnControlProductos_Click(sender As Object, e As EventArgs) Handles btnControlProductos.Click
        PrincipalControlDeProductos.ShowDialog(Me)
    End Sub

    Private Sub BtnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click
        Dispose()
    End Sub

    Private Sub BtnAdquisicion_Click(sender As Object, e As EventArgs) Handles btnAdquisicion.Click
        PrincipalAdquisiciones.ShowDialog(Me)
    End Sub
End Class