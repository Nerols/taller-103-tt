﻿Public Class PrincipalProveedores
    Dim bsnProveedor As New BSNProveedor

    Private Sub btnCrearProveedor_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        CrearProveedor.ShowDialog()
    End Sub

    Private Sub btnEditarProveedor_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        If dgvProveedores.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim proveedor As New Proveedor
            Dim data = bsnProveedor.GetProveedor(dgvProveedores.SelectedRows.Item(0).Cells(0).Value.ToString)

            If Not DBNull.Value.Equals(data.Item(7)) Then
                MessageBox.Show("No es posible editar un proveedor eliminado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            proveedor.id = data.item(0)
            proveedor.rut = data.item(1)
            proveedor.nombre = data.item(2)
            proveedor.descripcion = data.item(3)
            proveedor.fono = data.item(4)
            proveedor.email = data.item(5)
            proveedor.sitio = data.item(6)

            Dim frmEditarProveedores As New EditarProovedor(Me, proveedor)

            frmEditarProveedores.ShowDialog()
        End If
    End Sub

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub PrincipalProveedores_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvProveedores.DataSource = bsnProveedor.GetProveedores()
        dgvProveedores.Columns.Item(0).Visible = False
    End Sub

    Private Sub TxtBusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtBusqueda.TextChanged
        If chkBorrados.Checked = True Then
            If rbRut.Checked = True Then
                dgvProveedores.DataSource = bsnProveedor.BusquedaAllProveedorRut(txtBusqueda.Text)
            Else
                dgvProveedores.DataSource = bsnProveedor.BusquedaAllProveedorNombre(txtBusqueda.Text)
            End If
        Else
            If rbRut.Checked = True Then
                dgvProveedores.DataSource = bsnProveedor.BusquedaProveedorRut(txtBusqueda.Text)
            Else
                dgvProveedores.DataSource = bsnProveedor.BusquedaProveedorNombre(txtBusqueda.Text)
            End If
        End If
    End Sub

    Private Sub BtnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If dgvProveedores.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim proveedor As New Proveedor()
            Dim data = bsnProveedor.GetProveedor(dgvProveedores.SelectedRows.Item(0).Cells(0).Value.ToString)
            proveedor.id = data.item(0)
            If Not DBNull.Value.Equals(data.Item(7)) Then
                MessageBox.Show("El proveedor ya está eliminado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If MessageBox.Show("¿ Está seguro de eliminar este proveedor ?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                bsnProveedor.EliminarProveedor(proveedor.id)
                MessageBox.Show("Proveedor eliminado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                chkBorrados.Checked = False
                dgvProveedores.DataSource = bsnProveedor.GetProveedores()
            End If
        End If
    End Sub

    Private Sub ChkBorrados_CheckedChanged(sender As Object, e As EventArgs) Handles chkBorrados.CheckedChanged
        If chkBorrados.Checked = True Then
            dgvProveedores.DataSource = bsnProveedor.GetAllProveedores()
        Else
            dgvProveedores.DataSource = bsnProveedor.GetProveedores()
        End If
    End Sub

    Private Sub BtnRestaurar_Click(sender As Object, e As EventArgs) Handles btnRestaurar.Click
        If dgvProveedores.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim proveedor As New Proveedor()
            Dim data = bsnProveedor.GetProveedor(dgvProveedores.SelectedRows.Item(0).Cells(0).Value.ToString)
            proveedor.id = data.item(0)
            If DBNull.Value.Equals(data.Item(7)) Then
                MessageBox.Show("No es posible restaurar un proveedor que no ha sido eliminado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If MessageBox.Show("¿ Está seguro de restaurar este producto ?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                bsnProveedor.RestaurarProovedor(proveedor.id)
                MessageBox.Show("Producto restaurado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                chkBorrados.Checked = False
            End If
        End If
    End Sub
End Class
