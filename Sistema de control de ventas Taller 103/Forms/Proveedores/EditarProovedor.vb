﻿Public Class EditarProovedor
    Dim frmProveedores As PrincipalProveedores
    Dim proveedor As Proveedor
    Dim bsnProveedor As New BSNProveedor
    Public Sub New(frmProveedor As PrincipalProveedores, proveedor As Proveedor)

        InitializeComponent()
        Me.frmProveedores = frmProveedor
        Me.proveedor = proveedor

    End Sub
    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub


    Private Sub EditarProovedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtNombre.Text = proveedor.nombre
        txtDescripcion.Text = proveedor.descripcion
        txtFono.Text = proveedor.fono
        txtEmail.Text = proveedor.email
        txtSitio.Text = proveedor.sitio
    End Sub

    Private Sub BtnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        If txtNombre.Text = "" Or txtDescripcion.Text = "" Or txtFono.Text = "" Or txtEmail.Text = "" Or txtSitio.Text = "" Then
            MessageBox.Show("Campos en blanco, ingrese datos porfavor", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        proveedor.nombre = txtNombre.Text
        proveedor.descripcion = txtDescripcion.Text
        proveedor.fono = txtFono.Text
        proveedor.email = txtEmail.Text
        proveedor.sitio = txtSitio.Text

        bsnProveedor.EditarProveedor(proveedor)

        MessageBox.Show("Proveedor editado correctamente.", "Completado.")
        PrincipalProveedores.dgvProveedores.DataSource = bsnProveedor.GetProveedores()
        Dispose()
    End Sub
End Class
