﻿Public Class CrearProveedor
    Dim bsnProveedor As New BSNProveedor

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub BtnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        Dim proveedor As New Proveedor
        If Trim(txtRut.Text) = "" Or Trim(txtDigito.Text) = "" Or Trim(txtNombre.Text) = "" Or Trim(txtDescripcion.Text) = "" Or Trim(txtFono.Text = "") Or Trim(txtEmail.Text) = "" Or Trim(txtSitio.Text) = "" Then
            MessageBox.Show("Campos en blanco, ingrese datos porfavor", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        'Validación de longitud de RUT
        If txtRut.Text.Length < 7 Then
            MessageBox.Show("Rut demasiado corto, reingrese", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        'Validación de digito
        If txtDigito.Text <> "0" And txtDigito.Text <> "1" And txtDigito.Text <> "2" And txtDigito.Text <> "3" And txtDigito.Text <> "4" And txtDigito.Text <> "5" And txtDigito.Text <> "6" And txtDigito.Text <> "7" And txtDigito.Text <> "8" And txtDigito.Text <> "9" And txtDigito.Text <> "K" Then
            MessageBox.Show("El digito verificador no es valido", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If
        'Validación de rut
        If txtRut.Text.Length = 8 Then
            Dim total As Integer = 0
            total = total + (txtRut.Text.Substring(7, 1) * 2)
            total = total + (txtRut.Text.Substring(6, 1) * 3)
            total = total + (txtRut.Text.Substring(5, 1) * 4)
            total = total + (txtRut.Text.Substring(4, 1) * 5)
            total = total + (txtRut.Text.Substring(3, 1) * 6)
            total = total + (txtRut.Text.Substring(2, 1) * 7)
            total = total + (txtRut.Text.Substring(1, 1) * 2)
            total = total + (txtRut.Text.Substring(0, 1) * 3)
            Dim nRut As Integer = Math.Truncate(total / 11)
            Dim kRut As String = "A"
            nRut = nRut * 11
            nRut = total - nRut
            nRut = 11 - nRut
            If nRut = 11 Then
                nRut = 0
            End If
            If nRut = 10 Then
                kRut = "K"
            End If
            If nRut.ToString <> txtDigito.Text And kRut <> txtDigito.Text Then
                MessageBox.Show("El rut ingresado no es valido", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
        Else
            Dim total As Integer = 0
            total = total + (txtRut.Text.Substring(6, 1) * 2)
            total = total + (txtRut.Text.Substring(5, 1) * 3)
            total = total + (txtRut.Text.Substring(4, 1) * 4)
            total = total + (txtRut.Text.Substring(3, 1) * 5)
            total = total + (txtRut.Text.Substring(2, 1) * 6)
            total = total + (txtRut.Text.Substring(1, 1) * 7)
            total = total + (txtRut.Text.Substring(0, 1) * 2)
            Dim n As Integer = Math.Truncate(total / 11)
            n = n * 11
            n = total - n
            n = 11 - n
            If n = 11 Then
                n = 0
            End If
            If n = 10 Then
                n = "K"
            End If
            If n.ToString <> txtDigito.Text Then
                MessageBox.Show("El rut ingresado no es valido", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Exit Sub
            End If
        End If
        Dim rut As String
        rut = txtRut.Text & "-" & txtDigito.Text

        proveedor.rut = rut
        proveedor.nombre = txtNombre.Text
        proveedor.descripcion = txtDescripcion.Text
        proveedor.fono = txtFono.Text
        proveedor.email = txtEmail.Text
        proveedor.sitio = txtSitio.Text

        bsnProveedor.CrearProveedor(proveedor)

        MessageBox.Show("Proveedor guardado correctamente.", "Completado.")
        PrincipalProveedores.dgvProveedores.DataSource = bsnProveedor.GetProveedores()
        Dispose()
    End Sub
    Private Sub TxtRut_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtRut.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub
End Class