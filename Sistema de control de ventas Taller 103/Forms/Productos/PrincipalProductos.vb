﻿Public Class PrincipalProductos
    Dim bsnProducto As New BSNProducto

    Private Sub btnCrearProducto_Click(sender As Object, e As EventArgs) Handles btnCrearProducto.Click
        Dim frmCrearProducto As New CrearProducto(Me)

        frmCrearProducto.ShowDialog()
    End Sub

    Private Sub btnEditarProducto_Click(sender As Object, e As EventArgs) Handles btnEditarProducto.Click
        If dgvProductos.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else

            Dim producto As New Producto()
            Dim data = bsnProducto.GetProducto(dgvProductos.SelectedRows.Item(0).Cells(0).Value.ToString)

            If Not DBNull.Value.Equals(data.Item(7)) Then
                MessageBox.Show("No es posible editar un producto eliminado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            producto.id = data.Item(0)
            producto.idTipo = data.Item(1)
            producto.nombre = data.Item(2)
            producto.precio = data.Item(3)
            producto.costo = data.Item(4)
            producto.stock = data.Item(5)
            producto.stockComp = data.Item(6)

            Dim frmEditarProducto As New EditarProducto(Me, producto)

            frmEditarProducto.ShowDialog()
        End If
    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub Principal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvProductos.DataSource = bsnProducto.GetProductos()
        dgvProductos.Columns.Item(0).Visible = False
    End Sub

    Private Sub TxtBusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtBusqueda.TextChanged
        If chkBorrados.Checked = True Then
            dgvProductos.DataSource = bsnProducto.BusquedaAllProducto(txtBusqueda.Text)
        Else
            dgvProductos.DataSource = bsnProducto.BusquedaProducto(txtBusqueda.Text)
        End If

    End Sub

    Private Sub BtnEliminarProducto_Click(sender As Object, e As EventArgs) Handles btnEliminarProducto.Click
        If dgvProductos.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim producto As New Producto()
            Dim data = bsnProducto.GetProducto(dgvProductos.SelectedRows.Item(0).Cells(0).Value.ToString)
            producto.id = data.item(0)
            If Not DBNull.Value.Equals(data.Item(7)) Then
                MessageBox.Show("El producto ya está eliminado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If MessageBox.Show("¿ Está seguro de eliminar este producto ?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                bsnProducto.EliminarProducto(producto.id)
                MessageBox.Show("Producto eliminado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                chkBorrados.Checked = False
                dgvProductos.DataSource = bsnProducto.GetProductos()
            End If
        End If
    End Sub

    Private Sub ChkBorrados_CheckedChanged(sender As Object, e As EventArgs) Handles chkBorrados.CheckedChanged
        If chkBorrados.Checked = True Then
            dgvProductos.DataSource = bsnProducto.GetAllProductos()
        Else
            dgvProductos.DataSource = bsnProducto.GetProductos()
        End If
    End Sub

    Private Sub BtnRestaurar_Click(sender As Object, e As EventArgs) Handles btnRestaurar.Click
        If dgvProductos.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim producto As New Producto()
            Dim data = bsnProducto.GetProducto(dgvProductos.SelectedRows.Item(0).Cells(0).Value.ToString)
            producto.id = data.item(0)
            If DBNull.Value.Equals(data.Item(7)) Then
                MessageBox.Show("No es posible restaurar un producto que no ha sido eliminado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If MessageBox.Show("¿ Está seguro de restaurar este producto ?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                bsnProducto.RestaurarProducto(producto.id)
                MessageBox.Show("Producto restaurado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                chkBorrados.Checked = False
            End If
        End If
    End Sub
End Class
