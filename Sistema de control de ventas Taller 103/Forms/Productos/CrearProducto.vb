﻿Public Class CrearProducto
    Dim bsnProducto As New BSNProducto
    Dim bsnTipo As New BSNTipo
    Dim frmProductos As PrincipalProductos

    Public Sub New(frmProductos As PrincipalProductos)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.frmProductos = frmProductos

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub BtnCrearProducto_Click(sender As Object, e As EventArgs) Handles btnCrearProducto.Click
        If (Trim(txtPrecio.Text) = "" Or Trim(txtCosto.Text) = "" Or Trim(txtStock.Text) = "" Or cmbTipo.SelectedIndex = -1) Then
            MessageBox.Show("Verifique los datos e inténtelo nuevamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        Dim producto As New Producto

        producto.idTipo = cmbTipo.SelectedValue
        producto.nombre = Trim(txtNombre.Text)

        producto.precio = txtPrecio.Text
        Producto.costo = txtCosto.Text
        Producto.stock = txtStock.Text

        If (bsnProducto.CrearProducto(Producto)) Then
            MessageBox.Show("Producto ingresado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)

            frmProductos.dgvProductos.DataSource = bsnProducto.GetProductos

            Dispose()
        Else
            MessageBox.Show("Verifique los datos e inténtelo nuevamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub CrearProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cmbTipo.DataSource = bsnTipo.GetTipos()
        cmbTipo.ValueMember = "ID"
        cmbTipo.DisplayMember = "NOMBRE"
    End Sub

    Private Sub txtPrecio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecio.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtStock_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtStock.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtCosto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCosto.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub
End Class