﻿Public Class EditarProducto
    Dim bsnProducto As New BSNProducto
    Dim bsnTipo As New BSNTipo
    Dim frmProductos As PrincipalProductos
    Dim producto As Producto

    Public Sub New(frmProductos As PrincipalProductos, producto As Producto)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.frmProductos = frmProductos
        Me.producto = producto
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub BtnModificar_Click(sender As Object, e As EventArgs) Handles btnModificar.Click
        If (Trim(txtPrecio.Text) = "" Or Trim(txtCosto.Text) = "" Or Trim(txtStock.Text) = "" Or cmbTipo.SelectedIndex = -1) Then
            MessageBox.Show("Verifique los datos e inténtelo nuevamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        producto.nombre = Trim(txtNombre.Text)
        producto.idTipo = cmbTipo.SelectedValue
        producto.precio = txtPrecio.Text
        producto.costo = txtCosto.Text
        producto.stock = txtStock.Text

        If (bsnProducto.EditarProducto(producto)) Then
            MessageBox.Show("Producto ingresado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)

            frmProductos.dgvProductos.DataSource = bsnProducto.GetProductos

            Dispose()
        Else
            MessageBox.Show("Verifique los datos e inténtelo nuevamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub EditarProducto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cmbTipo.DataSource = bsnTipo.GetTipos()
        cmbTipo.ValueMember = "ID"
        cmbTipo.DisplayMember = "NOMBRE"

        txtNombre.Text = producto.nombre
        cmbTipo.SelectedValue = producto.idTipo
        txtPrecio.Text = producto.precio
        txtStock.Text = producto.stock
        txtCosto.Text = producto.costo
    End Sub

    Private Sub txtPrecio_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtStock_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtCosto_KeyPress(sender As Object, e As KeyPressEventArgs)
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub
End Class
