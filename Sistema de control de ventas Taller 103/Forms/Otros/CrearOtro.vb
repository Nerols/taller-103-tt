﻿Public Class CrearOtro
    Dim frmOtros As PrincipalOtros
    Dim bsnOtro As New BSNOtro
    Dim bsnVenta As New BSNVenta

    Public Sub New(frmOtros As PrincipalOtros)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.frmOtros = frmOtros

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub BtnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        If (txtPrecio.Text = "") Or txtDescripcion.Text = "" Or cmbVenta.SelectedIndex = -1 Then
            MessageBox.Show("Verifique los datos e inténtelo nuevamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        Dim otro As New Otro

        otro.idVenta = cmbVenta.SelectedValue
        otro.descripcion = txtDescripcion.Text
        otro.precio = txtPrecio.Text

        If (bsnOtro.CrearOtro(otro)) Then
            MessageBox.Show("Registro ingresado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)

            frmOtros.dgvOtros.DataSource = bsnOtro.GetOtros()

            Dispose()
        Else
            MessageBox.Show("Verifique los datos e inténtelo nuevamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub txtPrecio_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecio.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub CrearOtro_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cmbVenta.DataSource = bsnVenta.GetVentas()
        cmbVenta.ValueMember = "Número Venta"
    End Sub

    Private Sub TxtPrecio_TextChanged(sender As Object, e As EventArgs) Handles txtPrecio.TextChanged

    End Sub
End Class