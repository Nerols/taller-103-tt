﻿Public Class PrincipalOtros
    Dim bsnOtro As New BSNOtro

    Private Sub BtnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        Dim frmCrearOtro As New CrearOtro(Me)

        frmCrearOtro.ShowDialog()
    End Sub

    Private Sub BtnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        If dgvOtros.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim otro As New Otro()
            Dim data = bsnOtro.GetOtro(dgvOtros.SelectedRows.Item(0).Cells(0).Value.ToString)

            otro.id = data.Item(0)
            otro.idVenta = data.Item(1)
            otro.descripcion = data.Item(2)
            otro.precio = data.Item(3)


            Dim frmEditarOtro As New EditarOtro(Me, otro)

            frmEditarOtro.ShowDialog()
        End If
    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub PrincipalOtros_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvOtros.DataSource = bsnOtro.GetOtros()
        dgvOtros.Columns.Item(0).Visible = False
    End Sub

    Private Sub BtnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If dgvOtros.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            If MessageBox.Show("¿ Está seguro de eliminar este Otro ?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                Dim otro As New Otro()
                Dim data = bsnOtro.GetOtro(dgvOtros.SelectedRows.Item(0).Cells(0).Value.ToString)

                otro.id = data.Item(0)
                bsnOtro.EliminarOtro(otro.id)

                dgvOtros.DataSource = bsnOtro.GetOtros

                MessageBox.Show("Otro eliminado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub
End Class