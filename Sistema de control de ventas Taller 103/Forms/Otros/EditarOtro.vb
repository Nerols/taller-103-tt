﻿Public Class EditarOtro
    Dim bsnOtro As New BSNOtro
    Dim bsnVenta As New BSNVenta
    Dim frmOtros As PrincipalOtros
    Dim otro As Otro

    Public Sub New(frmOtros As PrincipalOtros, otro As Otro)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.frmOtros = frmOtros
        Me.otro = otro

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub EditarOtro_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cmbVenta.DataSource = bsnVenta.GetVentas()
        cmbVenta.ValueMember = "Número venta"

        cmbVenta.SelectedValue = otro.idVenta
        txtDescripcion.Text = otro.descripcion
        txtPrecio.Text = otro.precio
    End Sub
    Private Sub txtPrecio_KeyPress(sender As Object, e As KeyPressEventArgs)

    End Sub

    Private Sub BtnEditar_Click_1(sender As Object, e As EventArgs) Handles btnEditar.Click
        If (txtPrecio.Text = "") Or txtDescripcion.Text = "" Or cmbVenta.SelectedIndex = -1 Then
            MessageBox.Show("Verifique los datos e inténtelo nuevamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        otro.idVenta = cmbVenta.SelectedValue
        otro.descripcion = Trim(txtDescripcion.Text)
        otro.precio = txtPrecio.Text

        If (bsnOtro.EditarOtro(otro)) Then
            MessageBox.Show("Registro editado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)

            frmOtros.dgvOtros.DataSource = bsnOtro.GetOtros()

            Dispose()
        Else
            MessageBox.Show("Verifique los datos e inténtelo nuevamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub BtnCerrar_Click_1(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub
End Class