﻿Public Class PrincipalTipos
    Dim bsnTipo As New BSNTipo

    Private Sub BtnCrear_Click(sender As Object, e As EventArgs) Handles btnCrearTipo.Click
        Dim frmCrearTipos As New CrearTipo(Me)

        frmCrearTipos.ShowDialog()
    End Sub

    Private Sub BtnEditar_Click(sender As Object, e As EventArgs) Handles btnEditarTipo.Click
        If dgvTipos.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim tipo As New Tipo()

            If Not DBNull.Value.Equals(dgvTipos.SelectedRows.Item(0).Cells(2).Value) Then
                MessageBox.Show("No es posible editar un tipo eliminado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            tipo.id = dgvTipos.SelectedRows.Item(0).Cells(0).Value
            tipo.nombre = dgvTipos.SelectedRows.Item(0).Cells(1).Value
            Dim frmEditarTipo As New EditarTipo(Me, tipo)
            frmEditarTipo.ShowDialog()
        End If

    End Sub

    Private Sub BtnVolver_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Me.Dispose()
    End Sub

    Private Sub PrincipalTipos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvTipos.DataSource = bsnTipo.GetTipos()
        dgvTipos.Columns.Item(0).Visible = False
    End Sub

    Private Sub BtnEliminarTipo_Click(sender As Object, e As EventArgs) Handles btnEliminarTipo.Click
        If dgvTipos.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim tipo As New Tipo()
            Dim data = bsnTipo.GetTipo(dgvTipos.SelectedRows.Item(0).Cells(0).Value.ToString)
            tipo.id = data.item(0)
            If Not DBNull.Value.Equals(data.Item(2)) Then
                MessageBox.Show("El tipo ya está eliminado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If MessageBox.Show("¿ Está seguro de eliminar este tipo ?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                bsnTipo.EliminarTipo(tipo.id)
                MessageBox.Show("Tipo eliminado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                chkBorrados.Checked = False
                dgvTipos.DataSource = bsnTipo.GetTipos()
            End If
        End If
    End Sub

    Private Sub TxtBusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtBusqueda.TextChanged
        If chkBorrados.Checked = True Then
            dgvTipos.DataSource = bsnTipo.BusquedaAllTipo(txtBusqueda.Text)
        Else
            dgvTipos.DataSource = bsnTipo.BusquedaTipo(txtBusqueda.Text)
        End If
    End Sub

    Private Sub ChkBorrados_CheckedChanged(sender As Object, e As EventArgs) Handles chkBorrados.CheckedChanged
        If chkBorrados.Checked = True Then
            dgvTipos.DataSource = bsnTipo.GetAllTipos()
        Else
            dgvTipos.DataSource = bsnTipo.GetTipos()
        End If
    End Sub

    Private Sub BtnRestaurar_Click(sender As Object, e As EventArgs) Handles btnRestaurar.Click
        If dgvTipos.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim tipo As New Tipo()
            Dim data = bsnTipo.GetTipo(dgvTipos.SelectedRows.Item(0).Cells(0).Value.ToString)
            tipo.id = data.item(0)
            If DBNull.Value.Equals(data.Item(2)) Then
                MessageBox.Show("No es posible restaurar un tipo que no ha sido eliminado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If MessageBox.Show("¿ Está seguro de restaurar este tipo ?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                bsnTipo.RestaurarTipo(tipo.id)
                MessageBox.Show("Tipo restaurado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                chkBorrados.Checked = False
            End If
        End If
    End Sub
End Class