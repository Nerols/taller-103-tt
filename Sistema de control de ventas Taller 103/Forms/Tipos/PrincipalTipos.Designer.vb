﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PrincipalTipos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PrincipalTipos))
        Me.dgvTipos = New System.Windows.Forms.DataGridView()
        Me.btnCrearTipo = New System.Windows.Forms.Button()
        Me.btnEditarTipo = New System.Windows.Forms.Button()
        Me.btnEliminarTipo = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtBusqueda = New System.Windows.Forms.TextBox()
        Me.btnRestaurar = New System.Windows.Forms.Button()
        Me.chkBorrados = New System.Windows.Forms.CheckBox()
        CType(Me.dgvTipos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvTipos
        '
        Me.dgvTipos.AllowUserToAddRows = False
        Me.dgvTipos.AllowUserToDeleteRows = False
        Me.dgvTipos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvTipos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTipos.Location = New System.Drawing.Point(12, 42)
        Me.dgvTipos.MultiSelect = False
        Me.dgvTipos.Name = "dgvTipos"
        Me.dgvTipos.ReadOnly = True
        Me.dgvTipos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTipos.ShowEditingIcon = False
        Me.dgvTipos.Size = New System.Drawing.Size(450, 141)
        Me.dgvTipos.TabIndex = 2
        Me.dgvTipos.TabStop = False
        '
        'btnCrearTipo
        '
        Me.btnCrearTipo.Location = New System.Drawing.Point(38, 198)
        Me.btnCrearTipo.Name = "btnCrearTipo"
        Me.btnCrearTipo.Size = New System.Drawing.Size(75, 23)
        Me.btnCrearTipo.TabIndex = 1
        Me.btnCrearTipo.Text = "Crear"
        Me.btnCrearTipo.UseVisualStyleBackColor = True
        '
        'btnEditarTipo
        '
        Me.btnEditarTipo.Location = New System.Drawing.Point(119, 198)
        Me.btnEditarTipo.Name = "btnEditarTipo"
        Me.btnEditarTipo.Size = New System.Drawing.Size(75, 23)
        Me.btnEditarTipo.TabIndex = 2
        Me.btnEditarTipo.Text = "Editar"
        Me.btnEditarTipo.UseVisualStyleBackColor = True
        '
        'btnEliminarTipo
        '
        Me.btnEliminarTipo.Location = New System.Drawing.Point(200, 198)
        Me.btnEliminarTipo.Name = "btnEliminarTipo"
        Me.btnEliminarTipo.Size = New System.Drawing.Size(75, 23)
        Me.btnEliminarTipo.TabIndex = 3
        Me.btnEliminarTipo.Text = "Eliminar"
        Me.btnEliminarTipo.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCerrar.Location = New System.Drawing.Point(362, 198)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(75, 23)
        Me.btnCerrar.TabIndex = 5
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(71, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(114, 13)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Busqueda por nombre:"
        '
        'txtBusqueda
        '
        Me.txtBusqueda.Location = New System.Drawing.Point(191, 12)
        Me.txtBusqueda.MaxLength = 50
        Me.txtBusqueda.Name = "txtBusqueda"
        Me.txtBusqueda.ShortcutsEnabled = False
        Me.txtBusqueda.Size = New System.Drawing.Size(176, 20)
        Me.txtBusqueda.TabIndex = 0
        '
        'btnRestaurar
        '
        Me.btnRestaurar.Location = New System.Drawing.Point(281, 198)
        Me.btnRestaurar.Name = "btnRestaurar"
        Me.btnRestaurar.Size = New System.Drawing.Size(75, 23)
        Me.btnRestaurar.TabIndex = 4
        Me.btnRestaurar.Text = "Restaurar"
        Me.btnRestaurar.UseVisualStyleBackColor = True
        '
        'chkBorrados
        '
        Me.chkBorrados.AutoSize = True
        Me.chkBorrados.Location = New System.Drawing.Point(191, 228)
        Me.chkBorrados.Name = "chkBorrados"
        Me.chkBorrados.Size = New System.Drawing.Size(106, 17)
        Me.chkBorrados.TabIndex = 6
        Me.chkBorrados.Text = "Mostrar Borrados"
        Me.chkBorrados.UseVisualStyleBackColor = True
        '
        'PrincipalTipos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCerrar
        Me.ClientSize = New System.Drawing.Size(477, 251)
        Me.Controls.Add(Me.chkBorrados)
        Me.Controls.Add(Me.btnRestaurar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtBusqueda)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnEliminarTipo)
        Me.Controls.Add(Me.btnEditarTipo)
        Me.Controls.Add(Me.btnCrearTipo)
        Me.Controls.Add(Me.dgvTipos)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(493, 290)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(493, 290)
        Me.Name = "PrincipalTipos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Tipos de producto - Sistema de control de ventas Taller 103"
        CType(Me.dgvTipos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvTipos As DataGridView
    Friend WithEvents btnCrearTipo As Button
    Friend WithEvents btnEditarTipo As Button
    Friend WithEvents btnEliminarTipo As Button
    Friend WithEvents btnCerrar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtBusqueda As TextBox
    Friend WithEvents btnRestaurar As Button
    Friend WithEvents chkBorrados As CheckBox
End Class
