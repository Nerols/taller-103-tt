﻿Public Class CrearTipo
    Dim bsnTipo As New BSNTipo
    Dim frmTipos As PrincipalTipos

    Public Sub New(frmTipo As PrincipalTipos)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.frmTipos = frmTipo

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub BtnCrearTipo_Click(sender As Object, e As EventArgs) Handles btnCrearTipo.Click
        Dim tipo As New Tipo

        tipo.nombre = txtNombreTipo.Text

        If (bsnTipo.CrearTipo(tipo)) Then
            MessageBox.Show("Tipo de producto ingresado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)

            frmTipos.dgvTipos.DataSource = bsnTipo.GetTipos

            Dispose()
        Else
            MessageBox.Show("Verifique los datos e inténtelo nuevamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
End Class