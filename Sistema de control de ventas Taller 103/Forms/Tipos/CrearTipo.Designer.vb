﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CrearTipo
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.txtNombreTipo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCrearTipo = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtNombreTipo
        '
        Me.txtNombreTipo.Location = New System.Drawing.Point(76, 17)
        Me.txtNombreTipo.MaxLength = 30
        Me.txtNombreTipo.Name = "txtNombreTipo"
        Me.txtNombreTipo.ShortcutsEnabled = False
        Me.txtNombreTipo.Size = New System.Drawing.Size(231, 20)
        Me.txtNombreTipo.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Nombre:"
        '
        'btnCrearTipo
        '
        Me.btnCrearTipo.Location = New System.Drawing.Point(151, 52)
        Me.btnCrearTipo.Name = "btnCrearTipo"
        Me.btnCrearTipo.Size = New System.Drawing.Size(75, 23)
        Me.btnCrearTipo.TabIndex = 1
        Me.btnCrearTipo.Text = "Crear"
        Me.btnCrearTipo.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCerrar.Location = New System.Drawing.Point(232, 52)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(75, 23)
        Me.btnCerrar.TabIndex = 2
        Me.btnCerrar.Text = "Cerrar"
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'CrearTipo
        '
        Me.AcceptButton = Me.btnCrearTipo
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCerrar
        Me.ClientSize = New System.Drawing.Size(319, 87)
        Me.Controls.Add(Me.btnCerrar)
        Me.Controls.Add(Me.btnCrearTipo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtNombreTipo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(441, 156)
        Me.MinimizeBox = False
        Me.Name = "CrearTipo"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Crear tipo - Sistema de control de ventas Taller 103"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtNombreTipo As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents btnCrearTipo As Button
    Friend WithEvents btnCerrar As Button
End Class
