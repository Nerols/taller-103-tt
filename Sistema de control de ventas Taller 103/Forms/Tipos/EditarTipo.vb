﻿Public Class EditarTipo
    Dim bsnTipo As New BSNTipo
    Dim frmTipos As New PrincipalTipos
    Dim tipo As New Tipo

    Public Sub New(frmTipo As PrincipalTipos, tipo As Tipo)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.frmTipos = frmTipo
        Me.tipo = tipo

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub BtnModificar_Click(sender As Object, e As EventArgs) Handles btnModificarTipo.Click
        tipo.nombre = txtNombre.Text

        If (bsnTipo.EditarTipo(tipo)) Then
            MessageBox.Show("Tipo de producto modificado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)

            frmTipos.dgvTipos.DataSource = bsnTipo.GetTipos

            Dispose()
        Else
            MessageBox.Show("Verifique los datos e inténtelo nuevamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub EditarTipos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        txtNombre.Text = tipo.nombre
    End Sub
End Class