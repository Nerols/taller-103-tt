﻿Public Class EditarEnvio
    Dim bsnEnvio As New BSNEnvio
    Dim bsnVenta As New BSNVenta
    Dim frmEnvios As PrincipalEnvios
    Dim envio As Envio

    Public Sub New(frmEnvios As PrincipalEnvios, envio As Envio)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.frmEnvios = frmEnvios
        Me.envio = envio
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs)
        Dispose()
    End Sub

    Private Sub trampaCamposNumericos(sender As Object, e As KeyPressEventArgs)
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub BtnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        If (txtPrecio.Text = "" Or txtDescripcion.Text = "" Or txtCourier.Text = "" Or txtNumEnvio.Text = "" Or cmbVenta.SelectedIndex = -1) Then
            MessageBox.Show("Verifique los datos e inténtelo nuevamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        envio.idVenta = cmbVenta.SelectedValue
        envio.fechaEnvio = dtpFecha.Value
        envio.precio = txtPrecio.Text
        envio.numeroEnvio = txtNumEnvio.Text
        envio.courier = txtCourier.Text
        envio.descripcion = txtDescripcion.Text

        If (bsnEnvio.EditarEnvio(envio)) Then
            MessageBox.Show("Registro editado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)

            frmEnvios.dgvEnvios.DataSource = bsnEnvio.GetEnvios()

            Dispose()
        Else
            MessageBox.Show("Verifique los datos e inténtelo nuevamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub EditarEnvios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cmbVenta.DataSource = bsnVenta.GetVentas()
        cmbVenta.ValueMember = "Número venta"

        cmbVenta.SelectedValue = envio.idVenta
        dtpFecha.Value = envio.fechaEnvio
        txtPrecio.Text = envio.precio
        txtNumEnvio.Text = envio.numeroEnvio
        txtCourier.Text = envio.courier
        txtDescripcion.Text = envio.descripcion
    End Sub
End Class