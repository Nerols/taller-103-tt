﻿Public Class CrearEnvio
    Dim bsnVenta As New BSNVenta
    Dim bsnEnvio As New BSNEnvio
    Dim frmEnvios As PrincipalEnvios

    Public Sub New(frmEnvios As PrincipalEnvios)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.frmEnvios = frmEnvios
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs)
        Dispose()
    End Sub

    Private Sub CrearEnvios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cmbVenta.DataSource = bsnVenta.GetVentas()
        cmbVenta.ValueMember = "Número Venta"
    End Sub

    Private Sub trampaCamposNumericos(sender As Object, e As KeyPressEventArgs) Handles txtPrecio.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub BtnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        If (txtPrecio.Text = "" Or txtDescripcion.Text = "" Or txtCourier.Text = "" Or txtNumEnvio.Text = "" Or cmbVenta.SelectedIndex = -1) Then
            MessageBox.Show("Verifique los datos e inténtelo nuevamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Return
        End If

        Dim envio As New Envio
        envio.idVenta = cmbVenta.SelectedValue
        envio.fechaEnvio = dtpFecha.Value
        envio.precio = txtPrecio.Text
        envio.numeroEnvio = txtNumEnvio.Text
        envio.courier = txtCourier.Text
        envio.descripcion = txtDescripcion.Text

        If (bsnEnvio.CrearEnvio(envio)) Then
            MessageBox.Show("Registro ingresado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)

            frmEnvios.dgvEnvios.DataSource = bsnEnvio.GetEnvios()

            Dispose()
        Else
            MessageBox.Show("Verifique los datos e inténtelo nuevamente", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
End Class