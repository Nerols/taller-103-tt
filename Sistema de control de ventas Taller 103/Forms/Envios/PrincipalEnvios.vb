﻿Public Class PrincipalEnvios
    Dim bsnEnvio As New BSNEnvio
    Private Sub BtnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        Dim frmCrearEnvio As New CrearEnvio(Me)

        frmCrearEnvio.ShowDialog()
    End Sub

    Private Sub BtnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        If dgvEnvios.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim envio As New Envio()
            Dim data = bsnEnvio.GetEnvio(dgvEnvios.SelectedRows.Item(0).Cells(0).Value.ToString)

            envio.id = data.Item(0)
            envio.fechaEnvio = data.Item(1)
            envio.precio = data.Item(2)
            envio.numeroEnvio = data.Item(3)
            envio.courier = data.Item(4)
            envio.descripcion = data.Item(5)
            envio.idVenta = data.Item(6)


            Dim frmEditarenvio As New EditarEnvio(Me, envio)

            frmEditarenvio.ShowDialog()
        End If
    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub PrincipalEnvios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvEnvios.DataSource = bsnEnvio.GetEnvios()
        dgvEnvios.Columns.Item(0).Visible = False
    End Sub

    Private Sub BtnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If dgvEnvios.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            If MessageBox.Show("¿ Está seguro de eliminar este envío ?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                Dim envio As New Envio()
                Dim data = bsnEnvio.GetEnvio(dgvEnvios.SelectedRows.Item(0).Cells(0).Value.ToString)

                envio.id = data.Item(0)
                bsnEnvio.EliminarEnvio(envio.id)

                dgvEnvios.DataSource = bsnEnvio.GetEnvios

                MessageBox.Show("Envío eliminado correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        End If
    End Sub
End Class