﻿Public Class CrearAdquisicion
    Dim bsnProducto As New BSNProducto
    Dim bsnProveedor As New BSNProveedor
    Dim bsnAdquisicion As New BSNAdquisicion
    Dim bsnItemAdquisicion As New BSNItemAdquisicion
    Dim num As Integer = 0
    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub CrearAdquisicion_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Carga de productos
        dgvProductos.DataSource = bsnProducto.GetProductos()
        dgvProductos.Columns.Item(0).Visible = False
        dgvProductos.Columns.Item(7).Visible = False

        ' Carga de proveedores
        cbProveedor.DataSource = bsnProveedor.GetProveedores()
        cbProveedor.ValueMember = "ID"
        cbProveedor.DisplayMember = "Nombre proveedor"

        ' Columnas lista

        dgvLista.Columns.Clear()

        Dim col As New DataGridViewTextBoxColumn

        col.Name = "ID Producto"
        dgvLista.Columns.Add(col)

        Dim col1 As New DataGridViewTextBoxColumn

        col1.Name = "Producto"
        dgvLista.Columns.Add(col1)

        Dim col2 As New DataGridViewTextBoxColumn

        col2.Name = "Cantidad"
        dgvLista.Columns.Add(col2)

        Dim col3 As New DataGridViewTextBoxColumn

        col3.Name = "Costo unitario"
        dgvLista.Columns.Add(col3)

        Dim col4 As New DataGridViewTextBoxColumn

        col4.Name = "Total"
        dgvLista.Columns.Add(col4)

        dgvLista.Columns.Item(0).Visible = False

    End Sub

    Private Sub TxtCantidad_Keypress(sender As Object, e As KeyPressEventArgs) Handles txtCantidad.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub TxtCosto_Keypress(sender As Object, e As KeyPressEventArgs) Handles txtCosto.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub BtnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        Dim adquisicion As New Adquisicion
        Dim itemAdquisicion As New ItemAdquisicion
        Dim producto As New Producto

        If cbProveedor.SelectedIndex = -1 Then
            MessageBox.Show("Proveedor seleccionado no valido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If
        If dgvLista.Rows.Count = 0 Then
            MessageBox.Show("Debe añadir al menos un producto.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        Dim dateString As String
        dateString = dtpFecha.Value.ToString("dd-MM-yyyy")

        adquisicion.idProveedor = cbProveedor.SelectedValue()
        adquisicion.fechaAdquisicion = dateString

        If MessageBox.Show("¿Está seguro de realizar esta adquisición?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
            bsnAdquisicion.CrearAdquisicion(adquisicion)
            Dim dataSet = bsnAdquisicion.GetAdquisicionMax()
            itemAdquisicion.idAdquisicion = dataSet.item(0).ToString()

            For x = 0 To dgvLista.Rows.Count - 1
                itemAdquisicion.idProducto = dgvLista.Rows(x).Cells(0).Value
                itemAdquisicion.cantidad = dgvLista.Rows(x).Cells(2).Value
                itemAdquisicion.costo = dgvLista.Rows(x).Cells(3).Value

                Dim dataSetProd = bsnProducto.GetProducto(dgvLista.Rows(x).Cells(0).Value())
                producto.id = dataSetProd.item(0).ToString()
                producto.idTipo = dataSetProd.item(1).ToString()
                producto.nombre = dataSetProd.item(2).ToString()
                producto.precio = dataSetProd.item(3).ToString()
                producto.costo = dataSetProd.item(4).ToString()
                producto.stock = dataSetProd.item(5).ToString()
                producto.stockComp = dataSetProd.item(6).ToString()


                producto.stock = producto.stock + dgvLista.Rows(x).Cells(2).Value
                producto.costo = dgvLista.Rows(x).Cells(3).Value
                bsnProducto.EditarProducto(producto)

                bsnItemAdquisicion.CrearItemAdquisicion(itemAdquisicion)
            Next



            MessageBox.Show("Adquisición agregada correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
            PrincipalAdquisiciones.dgvAdquisicion.DataSource = bsnAdquisicion.GetAdquisiciones()
            Dispose()
        End If

    End Sub

    Private Sub BtnAñadir_Click(sender As Object, e As EventArgs) Handles btnAñadir.Click
        If (Trim(txtCosto.Text) = "" Or Trim(txtCantidad.Text = "")) Then
            MessageBox.Show("Campos en blanco, ingrese datos porfavor", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        If dgvProductos.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar un producto.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            If dgvLista.RowCount > 0 Then
                For x = 0 To dgvLista.RowCount - 1
                    If dgvProductos.SelectedCells(0).Value.ToString() = dgvLista.Rows(x).Cells(0).Value.ToString() Then
                        If dgvLista.Rows(x).Cells(3).Value.ToString() <> txtCosto.Text Then
                            MessageBox.Show("Se esta ingresando un producto con diferente costo", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Exit Sub
                        End If
                        dgvLista.Rows(x).Cells(2).Value = Integer.Parse(dgvLista.Rows(x).Cells(2).Value) + Integer.Parse(txtCantidad.Text)
                        dgvLista.Rows(x).Cells(4).Value = Integer.Parse(dgvLista.Rows(x).Cells(4).Value) + (Integer.Parse(txtCantidad.Text) * Integer.Parse(txtCosto.Text))
                        Exit Sub
                    End If
                Next
            End If
            dgvLista.Rows.Add()
            dgvLista.Rows(num).Cells(0).Value = dgvProductos.SelectedCells(0).Value.ToString()
            dgvLista.Rows(num).Cells(1).Value = dgvProductos.SelectedCells(1).Value.ToString()
            dgvLista.Rows(num).Cells(2).Value = txtCantidad.Text
            dgvLista.Rows(num).Cells(3).Value = txtCosto.Text
            dgvLista.Rows(num).Cells(4).Value = Integer.Parse(txtCantidad.Text) * Integer.Parse(txtCosto.Text)
            txtCantidad.Text = ""
            txtCosto.Text = ""
            num = num + 1
        End If
    End Sub

    Private Sub BtnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If dgvLista.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            dgvLista.Rows.RemoveAt(dgvLista.CurrentRow.Index)
            num = num - 1
        End If
    End Sub

    Private Sub DgvLista_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvLista.CellClick
        btnEliminar.Enabled = True
    End Sub

    Private Sub DgvProductos_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvProductos.CellClick
        btnEliminar.Enabled = False
    End Sub
End Class