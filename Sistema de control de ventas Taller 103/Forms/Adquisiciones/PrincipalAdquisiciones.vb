﻿Public Class PrincipalAdquisiciones
    Dim bsnAdquisicion As New BSNAdquisicion
    Dim bsnProveedor As New BSNProveedor
    Dim bsnItemAdquisicion As New BSNItemAdquisicion
    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub BtnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        CrearAdquisicion.ShowDialog()
    End Sub

    Private Sub PrincipalAdquisiciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvAdquisicion.DataSource = bsnAdquisicion.GetAdquisiciones()
        dgvAdquisicion.Columns.Item(0).Visible = False
    End Sub

    Private Sub BtnVerDetalles_Click(sender As Object, e As EventArgs) Handles btnVerDetalles.Click
        If dgvAdquisicion.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim adquisicion As New Adquisicion()
            Dim data = bsnAdquisicion.GetAdquisicion(dgvAdquisicion.SelectedRows.Item(0).Cells(0).Value.ToString)

            adquisicion.id = data.Item(0)
            adquisicion.idProveedor = data.Item(1)
            adquisicion.fechaAdquisicion = data.Item(2)

            Dim dataProveedor = bsnProveedor.GetProveedor(data.item(1))

            Dim dataset As DataTable = bsnItemAdquisicion.GetItemAdquisicionDeProducto(adquisicion.id)
            Dim frmPrincipalItemAdquisiciones As New PrincipalItemAdquisiciones(Me, dataset, data, dataProveedor)

            frmPrincipalItemAdquisiciones.ShowDialog()
        End If
    End Sub

    Private Sub TxtBusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtBusqueda.TextChanged
        If chkBorrados.Checked = True Then
            dgvAdquisicion.DataSource = bsnAdquisicion.BusquedaAllAdquisicion(txtBusqueda.Text)
        Else
            dgvAdquisicion.DataSource = bsnAdquisicion.BusquedaAdquisicion(txtBusqueda.Text)
        End If

    End Sub

    Private Sub BtnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If dgvAdquisicion.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim adquisicion As New Adquisicion()
            Dim data = bsnAdquisicion.GetAdquisicion(dgvAdquisicion.SelectedRows.Item(0).Cells(0).Value.ToString)
            adquisicion.id = data.item(0)
            If Not DBNull.Value.Equals(data.Item(3)) Then
                MessageBox.Show("La adquisición ya está eliminada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If MessageBox.Show("¿ Está seguro de eliminar esta adquisición ?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                bsnAdquisicion.EliminarAdquisicion(adquisicion.id)
                MessageBox.Show("Adquisición eliminada correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                chkBorrados.Checked = False
                dgvAdquisicion.DataSource = bsnAdquisicion.GetAdquisiciones()
            End If
        End If
    End Sub

    Private Sub ChkBorrados_CheckedChanged(sender As Object, e As EventArgs) Handles chkBorrados.CheckedChanged
        If chkBorrados.Checked = True Then
            dgvAdquisicion.DataSource = bsnAdquisicion.GetAllAdquisiciones()
        Else
            dgvAdquisicion.DataSource = bsnAdquisicion.GetAdquisiciones()
        End If
    End Sub

    Private Sub BtnRestaurar_Click(sender As Object, e As EventArgs) Handles btnRestaurar.Click
        If dgvAdquisicion.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim adquisicion As New Adquisicion()
            Dim data = bsnAdquisicion.GetAdquisicion(dgvAdquisicion.SelectedRows.Item(0).Cells(0).Value.ToString)
            adquisicion.id = data.item(0)
            If DBNull.Value.Equals(data.Item(3)) Then
                MessageBox.Show("No es posible restaurar una adquisición que no ha sido eliminada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If MessageBox.Show("¿ Está seguro de restaurar esta adquisición ?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                bsnAdquisicion.RestaurarAdquisicion(adquisicion.id)
                MessageBox.Show("Adquisición restaurada correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                chkBorrados.Checked = False
            End If
        End If
    End Sub
End Class