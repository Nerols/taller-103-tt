﻿Public Class PrincipalItemAdquisiciones
    Dim frmAdquisicion As PrincipalAdquisiciones
    Dim adquisicion As Adquisicion
    Dim bsnItemAdquisicion As BSNItemAdquisicion
    Dim itemAdquisicion As ItemAdquisicion
    Dim dataset As DataTable
    Dim bsnAdquisicion As BSNAdquisicion
    Dim bsnProveedor As BSNProveedor
    Dim data As Object
    Dim dataProveedor As Object

    Public Sub New(frmAdquisicion As PrincipalAdquisiciones, dataset As DataTable, data As Object, dataProveedor As Object)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.frmAdquisicion = frmAdquisicion
        Me.dataset = dataset
        Me.data = data
        Me.dataProveedor = dataProveedor
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub PrincipalItemAdquisiciones_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvItemAdquisiciones.Columns.Clear()

        dgvItemAdquisiciones.DataSource = dataset
        Dim col As New DataGridViewTextBoxColumn

        col.Name = "Total"
        dgvItemAdquisiciones.Columns.Add(col)
        For x = 0 To dgvItemAdquisiciones.Rows.Count - 1
            dgvItemAdquisiciones.Rows(x).Cells(5).Value = Integer.Parse(dgvItemAdquisiciones.Rows(x).Cells(3).Value) * (Integer.Parse(dgvItemAdquisiciones.Rows(x).Cells(4).Value))
        Next
        dgvItemAdquisiciones.Columns.Item(0).Visible = False
        dgvItemAdquisiciones.Columns.Item(2).Visible = False

        lblFecha.Text = data.item(2)
        lblNombreProveedor.Text = dataProveedor.item(2)
        lblRut.Text = dataProveedor.item(1)
        lblFono.Text = dataProveedor.item(3)
        lblEmail.Text = dataProveedor.item(4)
        lblSitio.Text = dataProveedor.item(5)

    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

End Class