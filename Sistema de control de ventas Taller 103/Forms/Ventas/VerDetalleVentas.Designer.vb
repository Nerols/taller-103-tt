﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class VerDetalleVentas
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvItemsVenta = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblFechaEntrega = New System.Windows.Forms.Label()
        Me.lblFechaCompromiso = New System.Windows.Forms.Label()
        Me.lblFechaEmision = New System.Windows.Forms.Label()
        Me.lblDireccionCliente = New System.Windows.Forms.Label()
        Me.lblEmailCliente = New System.Windows.Forms.Label()
        Me.lblFonosCliente = New System.Windows.Forms.Label()
        Me.lblNomCliente = New System.Windows.Forms.Label()
        Me.lblId = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.brnCerrar = New System.Windows.Forms.Button()
        CType(Me.dgvItemsVenta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvItemsVenta
        '
        Me.dgvItemsVenta.AllowUserToAddRows = False
        Me.dgvItemsVenta.AllowUserToDeleteRows = False
        Me.dgvItemsVenta.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvItemsVenta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvItemsVenta.Location = New System.Drawing.Point(311, 43)
        Me.dgvItemsVenta.MultiSelect = False
        Me.dgvItemsVenta.Name = "dgvItemsVenta"
        Me.dgvItemsVenta.ReadOnly = True
        Me.dgvItemsVenta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvItemsVenta.ShowEditingIcon = False
        Me.dgvItemsVenta.Size = New System.Drawing.Size(381, 235)
        Me.dgvItemsVenta.TabIndex = 8
        Me.dgvItemsVenta.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lblFechaEntrega)
        Me.GroupBox1.Controls.Add(Me.lblFechaCompromiso)
        Me.GroupBox1.Controls.Add(Me.lblFechaEmision)
        Me.GroupBox1.Controls.Add(Me.lblDireccionCliente)
        Me.GroupBox1.Controls.Add(Me.lblEmailCliente)
        Me.GroupBox1.Controls.Add(Me.lblFonosCliente)
        Me.GroupBox1.Controls.Add(Me.lblNomCliente)
        Me.GroupBox1.Controls.Add(Me.lblId)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 20)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(293, 258)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Propiedades"
        '
        'lblFechaEntrega
        '
        Me.lblFechaEntrega.AutoSize = True
        Me.lblFechaEntrega.Location = New System.Drawing.Point(88, 195)
        Me.lblFechaEntrega.Name = "lblFechaEntrega"
        Me.lblFechaEntrega.Size = New System.Drawing.Size(10, 13)
        Me.lblFechaEntrega.TabIndex = 15
        Me.lblFechaEntrega.Text = "-"
        '
        'lblFechaCompromiso
        '
        Me.lblFechaCompromiso.AutoSize = True
        Me.lblFechaCompromiso.Location = New System.Drawing.Point(107, 172)
        Me.lblFechaCompromiso.Name = "lblFechaCompromiso"
        Me.lblFechaCompromiso.Size = New System.Drawing.Size(10, 13)
        Me.lblFechaCompromiso.TabIndex = 14
        Me.lblFechaCompromiso.Text = "-"
        '
        'lblFechaEmision
        '
        Me.lblFechaEmision.AutoSize = True
        Me.lblFechaEmision.Location = New System.Drawing.Point(143, 169)
        Me.lblFechaEmision.Name = "lblFechaEmision"
        Me.lblFechaEmision.Size = New System.Drawing.Size(10, 13)
        Me.lblFechaEmision.TabIndex = 13
        Me.lblFechaEmision.Text = "-"
        '
        'lblDireccionCliente
        '
        Me.lblDireccionCliente.AutoEllipsis = True
        Me.lblDireccionCliente.Location = New System.Drawing.Point(63, 116)
        Me.lblDireccionCliente.Name = "lblDireccionCliente"
        Me.lblDireccionCliente.Size = New System.Drawing.Size(220, 22)
        Me.lblDireccionCliente.TabIndex = 12
        Me.lblDireccionCliente.Text = "-"
        '
        'lblEmailCliente
        '
        Me.lblEmailCliente.AutoSize = True
        Me.lblEmailCliente.Location = New System.Drawing.Point(46, 93)
        Me.lblEmailCliente.Name = "lblEmailCliente"
        Me.lblEmailCliente.Size = New System.Drawing.Size(10, 13)
        Me.lblEmailCliente.TabIndex = 11
        Me.lblEmailCliente.Text = "-"
        '
        'lblFonosCliente
        '
        Me.lblFonosCliente.AutoSize = True
        Me.lblFonosCliente.Location = New System.Drawing.Point(108, 71)
        Me.lblFonosCliente.Name = "lblFonosCliente"
        Me.lblFonosCliente.Size = New System.Drawing.Size(10, 13)
        Me.lblFonosCliente.TabIndex = 10
        Me.lblFonosCliente.Text = "-"
        '
        'lblNomCliente
        '
        Me.lblNomCliente.AutoSize = True
        Me.lblNomCliente.Location = New System.Drawing.Point(89, 47)
        Me.lblNomCliente.Name = "lblNomCliente"
        Me.lblNomCliente.Size = New System.Drawing.Size(10, 13)
        Me.lblNomCliente.TabIndex = 9
        Me.lblNomCliente.Text = "-"
        '
        'lblId
        '
        Me.lblId.AutoSize = True
        Me.lblId.Location = New System.Drawing.Point(64, 23)
        Me.lblId.Name = "lblId"
        Me.lblId.Size = New System.Drawing.Size(10, 13)
        Me.lblId.TabIndex = 8
        Me.lblId.Text = "-"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 195)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(80, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "Fecha Entrega:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(6, 172)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(100, 13)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "Fecha Compromiso:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 149)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(134, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Fecha emisión de la venta:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 116)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(55, 13)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Dirección:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 93)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 13)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "E-mail:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 71)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(99, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Fonos de contacto:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 47)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(82, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Nombre Cliente:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "No. Venta:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(311, 27)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(103, 13)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Items de esta venta:"
        '
        'brnCerrar
        '
        Me.brnCerrar.Location = New System.Drawing.Point(315, 308)
        Me.brnCerrar.Name = "brnCerrar"
        Me.brnCerrar.Size = New System.Drawing.Size(75, 23)
        Me.brnCerrar.TabIndex = 0
        Me.brnCerrar.Text = "Cerrar"
        Me.brnCerrar.UseVisualStyleBackColor = True
        '
        'VerDetalleVentas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(704, 343)
        Me.Controls.Add(Me.brnCerrar)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvItemsVenta)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(720, 424)
        Me.MinimizeBox = False
        Me.Name = "VerDetalleVentas"
        Me.ShowIcon = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Detalles de venta - Sistema de control de ventas Taller 103"
        CType(Me.dgvItemsVenta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgvItemsVenta As DataGridView
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents lblFechaEntrega As Label
    Friend WithEvents lblFechaCompromiso As Label
    Friend WithEvents lblFechaEmision As Label
    Friend WithEvents lblDireccionCliente As Label
    Friend WithEvents lblEmailCliente As Label
    Friend WithEvents lblFonosCliente As Label
    Friend WithEvents lblNomCliente As Label
    Friend WithEvents lblId As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents brnCerrar As Button
End Class
