﻿Public Class CrearVentas
    Dim frmVentas As PrincipalVentas
    Dim bsnProducto As New BSNProducto
    Dim bsnVenta As New BSNVenta
    Dim bsnCliente As New BSNCliente
    Dim bsnItemVenta As New BSNItemVenta
    Dim cliente As New Cliente
    Public contProductos = 0
    'Dim dragseldet = dgvProductosVenta.DataSource

    Public Sub New(frmVentas As PrincipalVentas)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.frmVentas = frmVentas
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub CrearVentas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvProductos.DataSource = bsnProducto.GetProductos()
        dgvProductos.Columns(0).Visible = False

        ' Columnas lista
        Dim colIdProducto As New DataGridViewTextBoxColumn

        colIdProducto.Name = "Id Producto"
        dgvProductosVenta.Columns.Add(colIdProducto)

        Dim colNombreProducto As New DataGridViewTextBoxColumn

        colNombreProducto.Name = "Producto"
        dgvProductosVenta.Columns.Add(colNombreProducto)

        Dim colCantidad As New DataGridViewTextBoxColumn

        colCantidad.Name = "Cantidad"
        dgvProductosVenta.Columns.Add(colCantidad)

        Dim colCantidadComp As New DataGridViewTextBoxColumn

        colCantidadComp.Name = "Cantidad comprometida"
        dgvProductosVenta.Columns.Add(colCantidadComp)

        Dim colPrecio As New DataGridViewTextBoxColumn

        colPrecio.Name = "Precio"
        dgvProductosVenta.Columns.Add(colPrecio)

        Dim colTotal As New DataGridViewTextBoxColumn

        colTotal.Name = "Total"
        dgvProductosVenta.Columns.Add(colTotal)

        Dim colDescripcion As New DataGridViewTextBoxColumn
        colDescripcion.Name = "Descripcion"
        dgvProductosVenta.Columns.Add(colDescripcion)



        dgvProductosVenta.Columns.Item(0).Visible = False
    End Sub

    Private Sub BtnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click
        Dim data = bsnCliente.GetClientePorRut(txtRut.Text)
        If data IsNot Nothing Then
            ' Recupero los datos guardados
            cliente.id = data.Item(0)
            cliente.rut = data.Item(1)
            cliente.nombre = data.Item(2)
            cliente.apellido = data.Item(3)
            cliente.fono1 = data.Item(4)
            cliente.fono2 = data.Item(5)
            cliente.email = data.Item(6)
            cliente.direccion = data.Item(7)

            ' Hago los datos visibles
            lblRutCliente.Text = cliente.rut
            lblNombreCliente.Text = cliente.nombre & " " & cliente.apellido
            lblFonosClientes.Text = cliente.fono1 & " - " & cliente.fono2
            lblEmailCliente.Text = cliente.email
            lblDireccionCliente.Text = cliente.direccion
        Else
            MessageBox.Show("Cliente no encontrado en la base de datos, agregue para concretar la venta.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub BtnAñadir_Click(sender As Object, e As EventArgs) Handles btnAñadir.Click
        If dgvProductos.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar un producto.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim producto As New Producto()
            Dim dataSetProd = bsnProducto.GetProducto(dgvProductos.SelectedCells(0).Value.ToString())
            producto.id = dataSetProd.item(0).ToString()
            producto.idTipo = dataSetProd.item(1).ToString()
            producto.nombre = dataSetProd.item(2).ToString()
            producto.precio = dataSetProd.item(3).ToString()
            producto.costo = dataSetProd.item(4).ToString()
            producto.stock = dataSetProd.item(5).ToString()
            producto.stockComp = dataSetProd.item(6).ToString()

            Dim frmCrearItemVenta As New CrearItemVenta(Me, producto)

            frmCrearItemVenta.ShowDialog()
        End If
    End Sub

    Private Sub trampaCamposNumericos(sender As Object, e As KeyPressEventArgs)
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub BtnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        If lblNombreCliente.Text = "-" Then
            MessageBox.Show("Por favor ingrese un rut de cliente para poder continuar.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        If dgvProductosVenta.Rows.Count = 0 Then
            MessageBox.Show("Debe añadir al menos un producto.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Exit Sub
        End If

        Dim venta As New Venta
        Dim itemVenta As New ItemVenta
        Dim producto As New Producto

        venta.idCliente = cliente.id
        venta.fechaEmision = Date.Now
        venta.fechaCompromiso = dtpFechaCompromiso.Value

        If MessageBox.Show("¿Confirmar Venta?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
            bsnVenta.CrearVenta(venta)
            Dim dataSet = bsnVenta.GetUltVenta()

            For x = 0 To dgvProductosVenta.Rows.Count - 1
                itemVenta.idProducto = dgvProductosVenta.Rows(x).Cells(0).Value
                itemVenta.idVenta = dataSet.item(0).ToString()
                itemVenta.cantidad = dgvProductosVenta.Rows(x).Cells(2).Value
                itemVenta.cantidadComp = dgvProductosVenta.Rows(x).Cells(3).Value
                itemVenta.descripcion = dgvProductosVenta.Rows(x).Cells(6).Value

                ' Obtengo mi producto para sacar los datos del producto original
                Dim dataSetProd = bsnProducto.GetProducto(itemVenta.idProducto)
                producto.id = dataSetProd.item(0).ToString()
                producto.idTipo = dataSetProd.item(1).ToString()
                producto.nombre = dataSetProd.item(2).ToString()
                producto.precio = dataSetProd.item(3).ToString()
                producto.costo = dataSetProd.item(4).ToString()
                producto.stock = dataSetProd.item(5).ToString()
                producto.stockComp = dataSetProd.item(6).ToString()

                ' Completo los datos faltantes para itemVenta
                itemVenta.precioVenta = producto.precio
                itemVenta.costoVenta = producto.costo

                ' Logica para los stock comprometidos
                If (producto.stock >= itemVenta.cantidad) Then
                    producto.stock = producto.stock - itemVenta.cantidad
                Else
                    producto.stock = producto.stock - itemVenta.cantidad
                    producto.stockComp += producto.stock * -1
                    itemVenta.cantidadComp = producto.stock * -1
                    producto.stock = 0
                End If
                bsnProducto.EditarProducto(producto)

                bsnItemVenta.CrearItemVenta(itemVenta)
            Next

            MessageBox.Show("Venta agregada correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
            frmVentas.dgvVentas.DataSource = bsnVenta.GetVentas()
            Dispose()
        End If
    End Sub

    Private Sub BtnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If dgvProductosVenta.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            dgvProductosVenta.Rows.RemoveAt(dgvProductosVenta.CurrentRow.Index)
            contProductos = contProductos - 1
        End If
    End Sub
End Class