﻿Public Class CrearItemVenta
    Dim producto As Producto
    Dim frmCrearVentas As CrearVentas

    Public Sub New(frmCrearVentas As CrearVentas, producto As Producto)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.frmCrearVentas = frmCrearVentas
        Me.producto = producto
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub CrearItemVenta_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblNomProducto.Text = producto.nombre
        lblPreProducto.Text = producto.precio
        lblCosProducto.Text = producto.costo
    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If (Trim(txtCantidad.Text = "") Or Trim(txtCantidad.Text) = "0") Then
            MessageBox.Show("Por favor ingrese una cantidad válida", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Exit Sub
        End If

        Dim dgrProducto As New DataGridViewRow()
        dgrProducto.CreateCells(frmCrearVentas.dgvProductosVenta)
        dgrProducto.Cells(0).Value = producto.id
        dgrProducto.Cells(1).Value = producto.nombre
        dgrProducto.Cells(2).Value = txtCantidad.Text
        If (producto.stock >= CInt(txtCantidad.Text)) Then
            dgrProducto.Cells(3).Value = 0
        Else
            dgrProducto.Cells(3).Value = (producto.stock - CInt(txtCantidad.Text)) * -1
        End If
        dgrProducto.Cells(4).Value = producto.precio
        dgrProducto.Cells(5).Value = CInt(txtCantidad.Text) * producto.precio
        dgrProducto.Cells(6).Value = txtDescripcion.Text
        frmCrearVentas.contProductos = frmCrearVentas.contProductos + 1

        If frmCrearVentas.dgvProductosVenta.RowCount > 0 Then
            For x = 0 To frmCrearVentas.dgvProductosVenta.RowCount - 1
                If dgrProducto.Cells(0).Value.ToString() = frmCrearVentas.dgvProductosVenta.Rows(x).Cells(0).Value.ToString() Then
                    frmCrearVentas.dgvProductosVenta.Rows(x).Cells(2).Value = Integer.Parse(frmCrearVentas.dgvProductosVenta.Rows(x).Cells(2).Value) + Integer.Parse(txtCantidad.Text)
                    frmCrearVentas.dgvProductosVenta.Rows(x).Cells(4).Value = Integer.Parse(frmCrearVentas.dgvProductosVenta.Rows(x).Cells(4).Value) + (Integer.Parse(txtCantidad.Text) * Integer.Parse(producto.precio))
                    Exit Sub
                End If
            Next
        End If
        frmCrearVentas.dgvProductosVenta.Rows.Add(dgrProducto)
    End Sub

    Private Sub trampaCamposNumericos(sender As Object, e As KeyPressEventArgs) Handles txtCantidad.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                e.Handled = True
            End If
        End If
    End Sub
End Class