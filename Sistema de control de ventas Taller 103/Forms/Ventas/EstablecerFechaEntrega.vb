﻿Public Class EstablecerFechaEntrega
    Dim frmVentas As PrincipalVentas
    Dim venta As New Venta
    Dim bsnVenta As New BSNVenta
    Dim sw As Integer = 0

    Public Sub New(frmVentas As PrincipalVentas, id As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Me.frmVentas = frmVentas
        Dim data = bsnVenta.GetVenta(id)
        venta.id = data.Item(0)
        venta.idCliente = data.Item(1)
        venta.fechaEmision = data.Item(2)
        venta.fechaCompromiso = data.Item(4)
        If Not DBNull.Value.Equals(data.item(3)) Then
            sw = 1
        End If
        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub

    Private Sub BtnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.Click
        If sw = 1 Then
            venta.fechaEntrega = dtpFechaEntrega.Value
            bsnVenta.EstablecerFechaEntrega(venta)
            MessageBox.Show("Fecha de entrega establecida correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)

            frmVentas.dgvVentas.DataSource = bsnVenta.GetVentas()
            Dispose()
            Exit Sub
        End If
        Dim bsnItemVenta As New BSNItemVenta
        Dim bsnProducto As New BSNProducto
        Dim producto As New Producto
        Dim dataItemsVenta = bsnItemVenta.GetItemsVentaDeVentaLimpio(venta.id)

        For x = 0 To dataItemsVenta.Rows.Count() - 1
            Dim dataProducto = bsnProducto.GetProducto(dataItemsVenta.Rows(x).Item(1))

            producto.id = dataProducto.Item(0)
            producto.idTipo = dataProducto.Item(1)
            producto.nombre = dataProducto.Item(2)
            producto.precio = dataProducto.Item(3)
            producto.costo = dataProducto.Item(4)
            producto.stock = dataProducto.Item(5)
            producto.stockComp = dataProducto.Item(6)

            producto.stock -= CInt(dataItemsVenta.Rows(x).Item(7))
            producto.stockComp -= CInt(dataItemsVenta.Rows(x).Item(7))

            bsnProducto.EditarProducto(producto)
        Next

        venta.fechaEntrega = dtpFechaEntrega.Value
        bsnVenta.EstablecerFechaEntrega(venta)
        MessageBox.Show("Fecha de entrega establecida correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)

        frmVentas.dgvVentas.DataSource = bsnVenta.GetVentas()
        Dispose()
    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub
End Class