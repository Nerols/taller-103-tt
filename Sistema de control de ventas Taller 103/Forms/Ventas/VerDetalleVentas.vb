﻿Public Class VerDetalleVentas
    Dim frmVentas As PrincipalVentas
    Dim venta As New Venta
    Dim cliente As New Cliente
    Dim bsnVenta As New BSNVenta
    Dim bsnCliente As New BSNCliente
    Dim bsnItemVenta As New BSNItemVenta

    Public Sub New(frmVentas As PrincipalVentas, id As String)

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()
        Dim dataVenta = bsnVenta.GetVenta(id)
        Me.frmVentas = frmVentas
        venta.id = dataVenta.Item(0)
        venta.idCliente = dataVenta.Item(1)
        venta.fechaEmision = dataVenta.Item(2)
        If Not DBNull.Value.Equals(dataVenta.Item(3)) Then
            venta.fechaEntrega = dataVenta.Item(3)
        End If
        venta.fechaCompromiso = dataVenta.Item(4)
        If Not DBNull.Value.Equals(dataVenta.Item(5)) Then
            venta.borradoEn = dataVenta.item(5)
        End If

        Dim dataCliente = bsnCliente.GetCliente(venta.idCliente)
        cliente.id = dataCliente.Item(0)
        cliente.rut = dataCliente.Item(1)
        cliente.nombre = dataCliente.Item(2)
        cliente.apellido = dataCliente.Item(3)
        cliente.fono1 = dataCliente.Item(4)
        cliente.fono2 = dataCliente.Item(5)
        cliente.email = dataCliente.Item(6)
        cliente.direccion = dataCliente.Item(7)
        If Not DBNull.Value.Equals(dataCliente.Item(8)) Then
            cliente.borradoEn = dataCliente.item(8)
        End If

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().

    End Sub
    Private Sub VerDetalleVentas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblId.Text = venta.id
        lblNomCliente.Text = cliente.nombre & " " & cliente.apellido
        lblFonosCliente.Text = cliente.fono1 & " - " & cliente.fono2
        lblEmailCliente.Text = cliente.email
        lblDireccionCliente.Text = cliente.direccion
        lblFechaEmision.Text = venta.fechaEmision.ToString("dd/MM/yyyy")
        lblFechaCompromiso.Text = venta.fechaCompromiso.ToString("dd/MM/yyyy")
        If Not venta.fechaEntrega = Nothing Then
            lblFechaEntrega.Text = venta.fechaEntrega.ToString("dd/MM/yyyy")
        End If

        dgvItemsVenta.DataSource = bsnItemVenta.GetItemsVentaDeVenta(venta.id)
        dgvItemsVenta.Columns(0).Visible = False
    End Sub

    Private Sub BrnCerrar_Click(sender As Object, e As EventArgs) Handles brnCerrar.Click
        Dispose()
    End Sub
End Class