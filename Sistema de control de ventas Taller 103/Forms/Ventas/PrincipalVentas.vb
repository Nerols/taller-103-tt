﻿Public Class PrincipalVentas
    Dim bsnVenta As New BSNVenta
    Private Sub BtnCrear_Click(sender As Object, e As EventArgs) Handles btnCrear.Click
        Dim frmCrearVenta As New CrearVentas(Me)

        frmCrearVenta.ShowDialog()
    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        Dispose()
    End Sub

    Private Sub PrincipalVentas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dgvVentas.DataSource = bsnVenta.GetVentas()
    End Sub

    Private Sub BtnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click
        If dgvVentas.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim venta As New Venta()
            Dim data = bsnVenta.GetVenta(dgvVentas.SelectedRows.Item(0).Cells(0).Value.ToString)
            venta.id = data.item(0)
            If Not DBNull.Value.Equals(data.Item(5)) Then
                MessageBox.Show("La venta ya está eliminada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If MessageBox.Show("¿ Está seguro de eliminar está venta ?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                bsnVenta.EliminarVenta(venta.id)
                MessageBox.Show("Venta eliminada correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                chkBorrados.Checked = False
                dgvVentas.DataSource = bsnVenta.GetVentas()
            End If
        End If
    End Sub

    Private Sub ChkBorrados_CheckedChanged(sender As Object, e As EventArgs) Handles chkBorrados.CheckedChanged
        If chkBorrados.Checked = True Then
            dgvVentas.DataSource = bsnVenta.GetAllVentas()
        Else
            dgvVentas.DataSource = bsnVenta.GetVentas()
        End If
    End Sub

    Private Sub BtnRestaurar_Click(sender As Object, e As EventArgs) Handles btnRestaurar.Click
        If dgvVentas.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim data = bsnVenta.GetVenta(dgvVentas.SelectedRows.Item(0).Cells(0).Value.ToString)

            If DBNull.Value.Equals(data.Item(5)) Then
                MessageBox.Show("No es posible restaurar una venta que no ha sido eliminada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If MessageBox.Show("¿ Está seguro de restaurar está venta ?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                bsnVenta.RestaurarVenta(data.item(0))
                MessageBox.Show("Venta restaurada correctamente.", "Completado", MessageBoxButtons.OK, MessageBoxIcon.Information)
                chkBorrados.Checked = False
            End If
        End If
    End Sub

    Private Sub TxtBusqueda_TextChanged(sender As Object, e As EventArgs) Handles txtBusqueda.TextChanged
        If chkBorrados.Checked = True Then
            dgvVentas.DataSource = bsnVenta.BusquedaAllVenta(txtBusqueda.Text)
        Else
            dgvVentas.DataSource = bsnVenta.BusquedaVenta(txtBusqueda.Text)
        End If
    End Sub

    Private Sub BtnDetalle_Click(sender As Object, e As EventArgs) Handles btnDetalle.Click
        If dgvVentas.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim frmVerDetalleVenta As New VerDetalleVentas(Me, dgvVentas.SelectedRows.Item(0).Cells(0).Value.ToString)
            frmVerDetalleVenta.ShowDialog()
        End If
    End Sub

    Private Sub BtnFechaEntrega_Click(sender As Object, e As EventArgs) Handles btnFechaEntrega.Click
        If dgvVentas.SelectedRows.Count = 0 Then
            MessageBox.Show("Debe seleccionar una fila.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Else
            Dim data = bsnVenta.GetVenta(dgvVentas.SelectedRows.Item(0).Cells(0).Value.ToString)
            If Not DBNull.Value.Equals(data.Item(5)) Then
                MessageBox.Show("La venta está eliminada", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
            If Not DBNull.Value.Equals(data.Item(3)) Then
                If MessageBox.Show("Esta venta ya tiene una fecha de entrega" & Chr(13) & "¿Está seguro que desea editar la fecha de entrega?", "ALERTA", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then
                    Dim frmEstablecerFechaEntrega As New EstablecerFechaEntrega(Me, data.Item(0))
                    frmEstablecerFechaEntrega.ShowDialog()
                End If
            Else
                Dim bsnItemVenta As New BSNItemVenta
                Dim bsnProducto As New BSNProducto
                Dim dataItemsVenta = bsnItemVenta.GetItemsVentaDeVentaLimpio(data.Item(0))

                For x = 0 To dataItemsVenta.Rows.Count() - 1
                    Dim dataProducto = bsnProducto.GetProducto(dataItemsVenta.Rows(x).Item(1))

                    If CInt(dataProducto.Item(5)) < dataItemsVenta.Rows(x).Item(7) Then
                        MessageBox.Show("Esta venta posee un producto que no tiene stock suficiente para ser entregado." & Chr(13) & "Producto: " & dataProducto.Item(2) & Chr(13) & "Stock actual: " & dataProducto.Item(5) & Chr(13) & "Cantidad necesaria: " & dataItemsVenta.Rows(x).Item(7), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Exit Sub
                    End If
                Next
                Dim frmEstablecerFechaEntrega As New EstablecerFechaEntrega(Me, data.Item(0))
                frmEstablecerFechaEntrega.ShowDialog()
            End If
        End If
    End Sub
End Class